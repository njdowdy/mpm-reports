## TODOs

## App Flow

- User can sign up, sign in, and sign out with email and password COMPLETE
  - requirement: reset password option for users who forget COMPLETE
  - Sign up should capture a first and last name (not part of supabase auth; insert to profile table directly on sign up with supabase trigger/function) COMPLETE
- Signed in user can edit settings to:
  - Join a department COMPLETE
  - add a profile image COMPLETE
  - update name COMPLETE
  - reset password COMPLETE
- Signed in user can view, sort, filter, their submitted activities COMPLETE
  - requirement: table view w/ fields: activity name, date, tags, and then a flex category(?) COMPLETE
- Signed in user can create, update, and delete their submitted activities
  - create activity
    - requirement: capture other parties, commit to db via server action COMPLETE
    - requirement: capture tags, commit to db via server action COMPLETE
    - requirement: capture activity record details, commit to db via server action COMPLETE
    - requirement: commit relationship between activity and tags to db via server action COMPLETE
    - requirement: commit relationship between activity and other parties to db via server action COMPLETE
    - requirement: form: other parties should be a multi-select dropdown including other department users (or all users for cross departmental activities?) COMPLETE
    - requirement: form: tags should be a multi-select dropdown including all tags (should be able to enter text for a tag that doesn't exist; OR generate a model with tag form to add it to the list of tags -- good optimistic update target) COMPLETE
    - requirement: form: activity details should be a text area COMPLETE
    - requirement: form: activity name should be a drop down with options from the user's departments only COMPLETE
    - requirement: form: depending on selected activity, quantity unit information should be displayed in a drop down menu for the user to select COMPLETE
    - requirement: form: depending on selected activity, additional fields may be displayed for the user to fill out (e.g., publication requires a publication name, author list, title, journal, doi, link) TODO
  - edit activity TODO
    - if user is not the owner of the activity, they should not be able to edit it
    - if activity tags are being edited, the relationship between the activity and the tags should be updated
    - if activity other parties are being edited, the relationship between the activity and the other parties should be updated
    - if activity details are being edited, the activity details should be updated
  - delete activity COMPLETE
- Admins should be able to create new activities and attach them to their department COMPLETE
- Super admin activities like deleting, editing activities, users can be done via Supabase interface for now
- Admins should be able to view all activities for their department(s) by submitter
- All users should be able to view reports by quarter
  - requirement: reports should summarize activities by department, codes, groups, etc
- Reminder emails should be sent to users before the end of the quarter to remind them to submit their activities
