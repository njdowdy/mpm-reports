import ModalClose from "@/components/ModalClose";

export interface ModalProps {
  children: React.ReactNode;
}

export default function Modal({ children }: ModalProps) {
  return (
    <div className="fixed inset-0 z-50 overflow-y-auto backdrop-blur-md backdrop-brightness-50">
      <div className="container flex h-full items-start px-4 pb-20 pt-32">
        <div className="relative mx-auto inline-block min-w-[400px] overflow-y-auto rounded-lg border bg-background px-2 pt-16 pb-12 align-bottom shadow-lg ">
          <ModalClose />
          <div className="container mx-auto flex flex-col justify-center space-y-6 ">
            {children}
          </div>
        </div>
      </div>
    </div>
  );
}
