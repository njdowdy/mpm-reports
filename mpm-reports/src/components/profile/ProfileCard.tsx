import { User } from "@/lib/db/schema/users";
import { createClient } from "@/lib/supabase/server";
import { Suspense } from "react";
export interface ProfileCardProps {
  user: User;
}

// TODO: allow for editing of user profile if user isOwner of profile
export default async function ProfileCard({ user }: ProfileCardProps) {
  return (
    <div className="min-h-[125px]">
      <p>
        <strong>Username:</strong> {user.username}
      </p>
      <p>
        <strong>Email:</strong> {user.email}
      </p>
      <p>
        <strong>First Name:</strong> {user.firstName}
      </p>
      <p>
        <strong>Last Name:</strong> {user.lastName}
      </p>
      <p>
        <strong>Role:</strong> {user.role}
      </p>
    </div>
  );
}
