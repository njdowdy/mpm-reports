import * as React from "react";
import { Button } from "../ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogOverlay,
  DialogPortal,
  DialogTitle,
  DialogTrigger,
} from "../ui/dialog";
import { toast } from "sonner";
import { MoreHorizontal } from "lucide-react";
import { ActivityRecord } from "@/lib/db/schema/activityRecords";
import { deleteActivityRecordAction } from "@/lib/actions/activityRecords";
import Link from "next/link";

const triggerDelete = async (id: string) => {
  //TODO: optimistic update
  const error = await deleteActivityRecordAction(id);
  if (error) {
    toast.warning("Something went wrong: " + error);
  }
  toast.error(`Record ${id} permanently deleted`);
};

//?: Follows example: https://codesandbox.io/embed/r9sq1q mixed with shadcn docs
export function DropdownDialog(rowItem: ActivityRecord) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="ghost" className="h-8 w-8 p-0">
          <span className="sr-only">Open menu</span>
          <MoreHorizontal className="h-4 w-4" />
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent sideOffset={5}>
        <DropdownMenuGroup>
          <DropdownMenuLabel>Modify Item</DropdownMenuLabel>
          <DropdownMenuItem>
            <Link href={`/activity-records/${rowItem.id}`}>Detail View</Link>
          </DropdownMenuItem>
          <DialogItem triggerChildren="Edit">
            <DialogTitle>Edit Record</DialogTitle>
            <DialogDescription>Edit this record below.</DialogDescription>
            {/* // TODO: Place ActivityRecordEditForm here */}
            <p>…</p>
            {/* //TODO: The following might be better as part of the form to kick off server action */}
            <div className="flex space-x-2 justify-end">
              <DialogClose asChild>
                <Button variant={"secondary"}>Cancel</Button>
              </DialogClose>
              <Button variant={"success"} size={"sm"}>
                Save
              </Button>
            </div>
          </DialogItem>
          <DialogItem triggerChildren="Delete">
            <DialogTitle>Confirm Record Deletion</DialogTitle>
            <DialogDescription className="text-pretty">
              Are you sure you want to delete this record? This action cannot be
              undone!
            </DialogDescription>
            <div className="flex space-x-2 justify-end">
              <DialogClose asChild>
                <Button variant={"secondary"}>Cancel</Button>
              </DialogClose>
              <DialogClose asChild>
                <Button
                  variant={"destructive"}
                  size={"sm"}
                  onClick={async () => await triggerDelete(rowItem.id)}
                >
                  Delete
                </Button>
              </DialogClose>
            </div>
          </DialogItem>
        </DropdownMenuGroup>

        {/* <DropdownMenuSeparator /> */}

        {/* <DropdownMenuGroup>
                    <DropdownMenuLabel >Regular items</DropdownMenuLabel>
                    <DropdownMenuItem >Duplicate</DropdownMenuItem>
                    <DropdownMenuItem >Copy</DropdownMenuItem>
                    <DropdownMenuItem >Save</DropdownMenuItem>
                </DropdownMenuGroup> */}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

type DialogItemProps = {
  triggerChildren: React.ReactNode;
  children: React.ReactNode;
  onSelect?: () => void;
  onOpenChange?: (open: boolean) => void;
} & React.ComponentPropsWithoutRef<typeof DropdownMenuItem>;

const DialogItem = React.forwardRef(
  (props: DialogItemProps, forwardedRef: React.ForwardedRef<null>) => {
    const { triggerChildren, children, onSelect, onOpenChange, ...itemProps } =
      props;
    return (
      <Dialog onOpenChange={onOpenChange}>
        <DialogTrigger asChild>
          <DropdownMenuItem
            {...itemProps}
            ref={forwardedRef}
            className="DropdownMenuItem cursor-pointer"
            onSelect={(event) => {
              event.preventDefault();
              onSelect && onSelect();
            }}
          >
            {triggerChildren}
          </DropdownMenuItem>
        </DialogTrigger>
        <DialogPortal>
          <DialogOverlay className="DialogOverlay" />
          <DialogContent className="DialogContent">{children}</DialogContent>
        </DialogPortal>
      </Dialog>
    );
  },
);
DialogItem.displayName = "DialogItem";
