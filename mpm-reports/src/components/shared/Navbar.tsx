import Link from "next/link";
import UserAvatar from "../auth/UserAvatar";
import { User } from "@/lib/db/schema/users";
import UserDropdownMenu from "./UserDropdownMenu";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";

export interface NavbarProps {
  user: User | null;
}

export default async function Navbar() {
  const user = await getCurrentUser();

  return (
    <>
      <header className="px-4 lg:px-6 h-14 flex items-center">
        <Link href="/">
          <h1 className="text-2xl font-bold">CREEPI Reports</h1>
        </Link>
        <nav className="ml-auto flex gap-4 sm:gap-6 items-center">
          <Link
            className="text-sm font-medium hover:underline underline-offset-4"
            href="/help"
          >
            Help
          </Link>
          {user ? (
            <UserDropdownMenu user={user}>
              <UserAvatar user={user} />
            </UserDropdownMenu>
          ) : (
            <Link
              className="text-sm font-medium hover:underline underline-offset-4"
              href="/sign-in"
              scroll={false}
            >
              Sign In
            </Link>
          )}
        </nav>
      </header>
    </>
  );
}
