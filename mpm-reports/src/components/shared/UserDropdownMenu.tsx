import "server-only";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import Link from "next/link";
import { User } from "@/lib/db/schema/users";
import SignOutButton from "../auth/SignOutButton";

export interface UserDropdownMenuProps {
  children: React.ReactNode;
  user: User;
}

export default function UserDropdownMenu({
  children,
  user,
}: UserDropdownMenuProps) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger>{children}</DropdownMenuTrigger>
      <DropdownMenuContent side="bottom" align="end">
        <DropdownMenuLabel className="text-right justify-end text-md">
          Actions
        </DropdownMenuLabel>
        <DropdownMenuItem className="text-right justify-end cursor-pointer">
          <Link href={`/report-activity`}>Report New Activity</Link>
        </DropdownMenuItem>
        <DropdownMenuItem
          asChild
          className="text-right justify-end cursor-pointer"
        >
          <Link href={`/activity-records`}>View Reported Activities</Link>
        </DropdownMenuItem>
        <DropdownMenuItem
          asChild
          className="text-right justify-end cursor-pointer"
        >
          <Link href={`/reports`}>View Quarterly Reports</Link>
        </DropdownMenuItem>
        <DropdownMenuItem
          asChild
          className="text-right justify-end cursor-pointer"
        >
          <Link href={`/reset-password`}>Change Password</Link>
        </DropdownMenuItem>
        <DropdownMenuSeparator />
        <DropdownMenuLabel className="text-right justify-end text-md">
          View
        </DropdownMenuLabel>
        <DropdownMenuItem
          asChild
          className="text-right justify-end cursor-pointer"
        >
          <Link href={`/profile/${user.username}`}>Profile</Link>
        </DropdownMenuItem>
        <DropdownMenuItem
          asChild
          className="text-right justify-end cursor-pointer"
        >
          <Link href={`/settings`}>Settings</Link>
        </DropdownMenuItem>
        {user.role === "admin" ||
          (user.role === "superuser" && (
            <>
              <DropdownMenuSeparator />
              <DropdownMenuLabel className="text-right justify-end text-md">
                Admin Options
              </DropdownMenuLabel>
              <DropdownMenuItem
                asChild
                className="text-right justify-end cursor-pointer"
              >
                <Link href={`/activities`}>Manage Department Activities</Link>
              </DropdownMenuItem>
            </>
          ))}
        {user.role === "superuser" && (
          <>
            <DropdownMenuSeparator />
            <DropdownMenuLabel className="text-right justify-end text-md">
              Super User Options
            </DropdownMenuLabel>
            <DropdownMenuItem
              asChild
              className="text-right justify-end cursor-pointer"
            >
              <Link href={`/users`}>Manage Users</Link>
            </DropdownMenuItem>
          </>
        )}
        <DropdownMenuSeparator />
        <DropdownMenuItem className="p-0 h-8 justify-end">
          <SignOutButton />
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
