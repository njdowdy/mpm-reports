"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { Cross1Icon } from "@radix-ui/react-icons";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { toast } from "sonner";
import { HTMLInputTypeAttribute, useCallback } from "react";
import { signInWithEmailAndPassword } from "@/lib/supabase/auth/server/actions";
import { useRouter } from "next/navigation";
import Link from "next/link";

// Define form schema
const formSchema = z.object({
  email: z
    .string()
    .email({
      message: "Please enter a valid email address",
    })
    .refine((e) => e.endsWith("@mpm.edu"), "Please use an @mpm.edu address"),
  password: z.string(),
});

export type SignInFormType = z.infer<typeof formSchema>;

export default function SignInForm() {
  const router = useRouter();

  const form = useForm<SignInFormType>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  // Define form fields
  type FormFields = {
    id: number;
    name: Exclude<keyof SignInFormType, "agreeToTerms">;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    autocomplete: AutoFillNormalField | "off";
    description: string;
  }[];

  const formFields: FormFields = [
    {
      id: 1,
      name: "email",
      label: "Email",
      type: "email",
      autocomplete: "off",
      placeholder: "email@mpm.edu",
      description: "",
    },
    {
      id: 2,
      name: "password",
      label: "Password",
      type: "password",
      autocomplete: "current-password",
      placeholder: "",
      description: "",
    },
  ];

  // Define submit handler
  async function onSubmit(formData: SignInFormType) {
    // TODO: check for validation success
    const { error } = JSON.parse(await signInWithEmailAndPassword(formData));
    if (error) {
      if (error.status === 400) {
        toast.error("Invalid email or password.");
        return;
      }

      if (error.message.includes("fetch failed")) {
        toast.error(
          "Failed to contact database. Check your internet connection.",
        );
        return;
      }

      toast.error(error.message);
      return;
    }
    toast.success(`You are now logged in!`, {
      duration: 2000,
      action: {
        // see: https://github.com/emilkowalski/sonner/pull/280
        // @ts-ignore-next-line
        label: (
          <>
            <Cross1Icon />
          </>
        ),
        onClick(event) {
          // console.log("toast.action.onClick", event);
        },
      },
      actionButtonStyle: {
        background: "transparent",
        color: "black",
      },
    });
    router.back();
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
        {formFields.map((formField) => (
          <FormField
            key={formField.id}
            control={form.control}
            name={formField.name}
            render={({ field }) => (
              <FormItem>
                <FormLabel>{formField.label}</FormLabel>
                <FormControl>
                  <Input
                    type={formField.type}
                    placeholder={formField.placeholder}
                    autoComplete={formField.autocomplete}
                    {...field}
                  />
                </FormControl>
                <FormDescription>{formField.description}</FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        <Link
          href="/reset-password-request"
          className="underline text-sm text-muted-foreground"
          prefetch={false}
        >
          Forgot your password?
        </Link>
        <Button id="loginMessage" className="w-full" size="lg" type="submit">
          Sign In
        </Button>
      </form>
    </Form>
  );
}
