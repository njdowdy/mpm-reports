"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { Cross1Icon } from "@radix-ui/react-icons";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { toast } from "sonner";
import { HTMLInputTypeAttribute, useCallback } from "react";
import { signUpWithEmail } from "@/lib/supabase/auth/server/actions";
import { useRouter } from "next/navigation";

// Define form schema
const formSchema = z
  .object({
    email: z
      .string()
      .email({
        message: "Please enter a valid email address",
      })
      .refine((e) => e.endsWith("@mpm.edu"), "Please use an @mpm.edu address"),
    firstName: z
      .string()
      .min(2, { message: "First name should be at least 2 characters long." }),
    lastName: z
      .string()
      .min(2, { message: "Last name should be at least 2 characters long." }),
    password: z
      .string()
      .min(8, {
        message: "Password must be at least 8 characters.",
      })
      .max(32, {
        message: "Password must be less than 32 characters.",
      })
      .regex(/(?=.*[a-z])/, {
        message: "Password must contain at least one lowercase letter.",
      })
      .regex(/(?=.*[A-Z])/, {
        message: "Password must contain at least one uppercase letter.",
      })
      .regex(/(?=.*\d)/, {
        message: "Password must contain at least one digit.",
      })
      .regex(/(?=.*[!@#$%^&*])/, {
        message:
          "Password must contain at least one of the following: !@#$%^&*",
      }),
    passwordConfirm: z.string(),
  })
  .refine((data) => data.password === data.passwordConfirm, {
    message: "Passwords must match.",
    path: ["passwordConfirm"],
  });

export type SignUpFormType = z.infer<typeof formSchema>;

export default function SignUpForm() {
  const router = useRouter();

  const form = useForm<SignUpFormType>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      firstName: "",
      lastName: "",
      password: "",
      passwordConfirm: "",
    },
  });

  // Define form fields
  type FormFields = {
    id: number;
    name: Exclude<keyof SignUpFormType, "agreeToTerms">;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    autocomplete: AutoFillNormalField | "off";
    description: string;
  }[];

  const formFields: FormFields = [
    {
      id: 1,
      name: "email",
      label: "Email",
      type: "email",
      autocomplete: "off",
      placeholder: "email@mpm.edu",
      description: "",
    },
    {
      id: 2,
      name: "firstName",
      label: "First Name",
      type: "text",
      autocomplete: "given-name",
      placeholder: "",
      description: "",
    },
    {
      id: 3,
      name: "lastName",
      label: "Last Name",
      type: "text",
      autocomplete: "family-name",
      placeholder: "",
      description: "",
    },
    {
      id: 4,
      name: "password",
      label: "Password",
      type: "password",
      autocomplete: "current-password",
      placeholder: "",
      description: "",
    },
    {
      id: 5,
      name: "passwordConfirm",
      label: "Confirm Password",
      type: "password",
      placeholder: "",
      description: "",
      autocomplete: "off",
    },
  ];

  // Define submit handler
  async function onSubmit(formData: SignUpFormType) {
    const { passwordConfirm, ...payload } = formData;
    // TODO: check for validation success
    const { error } = JSON.parse(await signUpWithEmail(payload));
    if (error) {
      if (error.status === 400) {
        toast.error("Something went wrong. Please try again.");
      } else {
        toast.error(error.message);
      }
      return;
    }
    toast.info(`Please check your email to complete your account creation!`, {
      duration: 2000,
      action: {
        // see: https://github.com/emilkowalski/sonner/pull/280
        // @ts-ignore-next-line
        label: (
          <>
            <Cross1Icon />
          </>
        ),
        onClick(event) {
          // console.log("toast.action.onClick", event);
        },
      },
      actionButtonStyle: {
        background: "transparent",
        color: "black",
      },
    });
    router.back();
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
        {formFields.map((formField) => (
          <FormField
            key={formField.id}
            control={form.control}
            name={formField.name}
            render={({ field }) => (
              <FormItem>
                <FormLabel>{formField.label}</FormLabel>
                <FormControl>
                  <Input
                    type={formField.type}
                    placeholder={formField.placeholder}
                    autoComplete={formField.autocomplete}
                    {...field}
                  />
                </FormControl>
                <FormDescription>{formField.description}</FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        <Button id="signUpMessage" className="w-full" size="lg" type="submit">
          Sign Up
        </Button>
      </form>
    </Form>
  );
}
