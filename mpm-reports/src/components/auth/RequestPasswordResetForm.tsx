"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { HTMLInputTypeAttribute } from "react";
import { toast } from "sonner";
import {
  requestPasswordResetWithEmail,
  signInWithEmailAndPassword,
} from "@/lib/supabase/auth/server/actions";
import { Cross1Icon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";

// Define form schema
const formSchema = z.object({
  email: z.string().email({
    message: "Please enter a valid email address",
  }),
  // .refine((e) => e.endsWith("@mpm.edu"), "Please use an @mpm.edu address"),
});

export type RequestPasswordResetFormType = z.infer<typeof formSchema>;

export default function RequestPasswordResetForm() {
  const router = useRouter();

  const form = useForm<RequestPasswordResetFormType>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
    },
  });

  // Define form fields
  type FormFields = {
    id: number;
    name: keyof RequestPasswordResetFormType;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    autocomplete: AutoFillNormalField | "off";
    description: string;
  }[];

  const formFields: FormFields = [
    {
      id: 1,
      name: "email",
      label: "Email",
      type: "email",
      autocomplete: "off",
      placeholder: "email@mpm.edu",
      description: "",
    },
  ];

  // Define submit handler
  async function onSubmit(formData: RequestPasswordResetFormType) {
    // TODO: check for validation success
    const res = JSON.parse(await requestPasswordResetWithEmail(formData));
    // console.log(JSON.stringify(res, null, 2));
    const { error } = res;
    if (error) {
      toast.error("Something went wrong. Please try again.");
      return;
    }
    toast.success(`Please check your email`, {
      duration: 2000,
      action: {
        // see: https://github.com/emilkowalski/sonner/pull/280
        // @ts-ignore-next-line
        label: (
          <>
            <Cross1Icon />
          </>
        ),
        onClick(event) {
          // console.log("toast.action.onClick", event);
        },
      },
      actionButtonStyle: {
        background: "transparent",
        color: "black",
      },
    });
    router.push("/");
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
        {formFields.map((formField) => (
          <FormField
            key={formField.id}
            control={form.control}
            name={formField.name}
            render={({ field }) => (
              <FormItem>
                <FormLabel>{formField.label}</FormLabel>
                <FormControl>
                  <Input
                    type={formField.type}
                    placeholder={formField.placeholder}
                    autoComplete={formField.autocomplete}
                    {...field}
                    value={field.value ?? ""}
                  />
                </FormControl>
                <FormDescription>{formField.description}</FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        <Button type="submit" size={"sm"}>
          Submit
        </Button>
      </form>
    </Form>
  );
}
