"use client";

import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { User } from "@/lib/db/schema/users";
import { FaUser } from "react-icons/fa";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";

import { cva, type VariantProps } from "class-variance-authority";
import { cn } from "@/lib/utils";
import Link from "next/link";

const avatarVariants = cva("cursor-pointer", {
  variants: {
    variant: {
      default: "",
      bordered: "border-2 border-muted-foreground/50",
    },
  },
  defaultVariants: {
    variant: "default",
  },
});

export interface UserAvatarProps extends VariantProps<typeof avatarVariants> {
  user: User;
  renderTooltip?: boolean;
}

// TODO: this could be tidied up some
export default function UserAvatar({
  user,
  renderTooltip,
  variant,
}: UserAvatarProps) {
  return (
    <Avatar className={cn(avatarVariants({ variant }))}>
      {renderTooltip ? (
        <TooltipProvider>
          <Tooltip delayDuration={100}>
            <TooltipTrigger>
              <AvatarImage src={user.avatarUrl || ""} />
            </TooltipTrigger>
            <TooltipContent className="z-50 shadow-lg">
              <Link href={`profile/${user.username}`}>
                {user.firstName} {user.lastName}
              </Link>
            </TooltipContent>
          </Tooltip>
        </TooltipProvider>
      ) : (
        <AvatarImage src={user.avatarUrl || ""} />
      )}
      {user.firstName && user.lastName ? (
        <AvatarFallback>
          {renderTooltip ? (
            <TooltipProvider>
              <Tooltip delayDuration={100}>
                <TooltipTrigger>
                  {user.firstName.charAt(0).toUpperCase() +
                    user.lastName.charAt(0).toUpperCase()}
                </TooltipTrigger>
                <TooltipContent className="z-50 shadow-lg">
                  <Link href={`profile/${user.username}`}>
                    {user.firstName} {user.lastName}
                  </Link>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          ) : (
            user.firstName.charAt(0).toUpperCase() +
            user.lastName.charAt(0).toUpperCase()
          )}
        </AvatarFallback>
      ) : (
        <AvatarFallback>
          {renderTooltip ? (
            <TooltipProvider>
              <Tooltip delayDuration={100}>
                <TooltipTrigger>
                  <FaUser />
                </TooltipTrigger>
                <TooltipContent className="z-50 shadow-lg">
                  <Link href={`profile/${user.username}`}>
                    {user.firstName} {user.lastName}
                  </Link>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          ) : (
            <FaUser />
          )}
        </AvatarFallback>
      )}
    </Avatar>
  );
}
