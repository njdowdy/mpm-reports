"use client";

import { signOut } from "@/lib/supabase/auth/server/actions";
import { Button } from "@/components/ui/button";
import { useRouter } from "next/navigation";

export default function SignOutButton() {
  const router = useRouter();

  async function handleSignOut() {
    await signOut();
    router.push("/");
  }

  return (
    <Button
      className="text-sm font-medium hover:underline underline-offset-4"
      onClick={async () => await handleSignOut()}
      variant={"link"}
    >
      Sign Out
    </Button>
  );
}
