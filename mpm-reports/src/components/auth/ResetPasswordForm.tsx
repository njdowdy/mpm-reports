"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { HTMLInputTypeAttribute } from "react";
import { toast } from "sonner";
import { Cross1Icon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import { resetSignedInUserPassword } from "@/lib/supabase/client";

// Define form schema
const formSchema = z
  .object({
    password: z
      .string()
      .min(8, {
        message: "Password must be at least 8 characters.",
      })
      .max(32, {
        message: "Password must be less than 32 characters.",
      })
      .regex(/(?=.*[a-z])/, {
        message: "Password must contain at least one lowercase letter.",
      })
      .regex(/(?=.*[A-Z])/, {
        message: "Password must contain at least one uppercase letter.",
      })
      .regex(/(?=.*\d)/, {
        message: "Password must contain at least one digit.",
      })
      .regex(/(?=.*[!@#$%^&*])/, {
        message:
          "Password must contain at least one of the following: !@#$%^&*",
      }),
    passwordConfirm: z.string(),
  })
  .refine((data) => data.password === data.passwordConfirm, {
    message: "Passwords must match.",
    path: ["passwordConfirm"],
  });

export type ResetPasswordFormType = z.infer<typeof formSchema>;

export default function ResetPasswordForm() {
  const router = useRouter();

  const form = useForm<ResetPasswordFormType>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      password: "",
      passwordConfirm: "",
    },
  });

  // Define form fields
  type FormFields = {
    id: number;
    name: keyof ResetPasswordFormType;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    autocomplete: AutoFillNormalField | "off";
    description: string;
  }[];

  const formFields: FormFields = [
    {
      id: 1,
      name: "password",
      label: "Password",
      type: "password",
      placeholder: "",
      description: "",
      autocomplete: "new-password",
    },
    {
      id: 2,
      name: "passwordConfirm",
      label: "Confirm Password",
      type: "password",
      placeholder: "",
      description: "",
      autocomplete: "off",
    },
  ];

  // Define submit handler
  async function onSubmit(formData: ResetPasswordFormType) {
    // TODO: check for validation success
    const { error } = JSON.parse(
      await resetSignedInUserPassword({ newPassword: formData.password }),
    );
    if (error) {
      if (error.message.includes("New password should be different")) {
        toast.error(
          "Please select a password that is different from your current password.",
        );
      } else {
        toast.error(error.message);
      }
      return;
    }

    toast.success(`Your password was changed!`, {
      duration: 2000,
      action: {
        // see: https://github.com/emilkowalski/sonner/pull/280
        // @ts-ignore-next-line
        label: (
          <>
            <Cross1Icon />
          </>
        ),
        onClick(event) {
          // console.log("toast.action.onClick", event);
        },
      },
      actionButtonStyle: {
        background: "transparent",
        color: "black",
      },
    });
    router.push("/");
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        {formFields.map((formField) => (
          <FormField
            key={formField.id}
            control={form.control}
            name={formField.name}
            render={({ field }) => (
              <FormItem>
                <FormLabel>{formField.label}</FormLabel>
                <FormControl>
                  <Input
                    type={formField.type}
                    placeholder={formField.placeholder}
                    autoComplete={formField.autocomplete}
                    {...field}
                  />
                </FormControl>
                <FormDescription>{formField.description}</FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        <Button type="submit">Reset</Button>
      </form>
    </Form>
  );
}
