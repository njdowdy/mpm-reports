import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { Suspense } from "react";
import SignInForm from "@/components/auth/SignInForm";
import SignUpForm from "@/components/auth/SignUpForm";

export default function AuthModal() {
  return (
    <Suspense
      fallback={
        <div className="flex flex-col items-center justify-center">
          Please wait...
        </div>
      }
    >
      <Tabs defaultValue="sign-in" className="w-[400px]">
        <TabsList className="grid w-full grid-cols-2">
          <TabsTrigger value="sign-in">Sign In</TabsTrigger>
          <TabsTrigger value="sign-up">Sign Up</TabsTrigger>
        </TabsList>
        <TabsContent value="sign-in">
          <Card>
            <CardHeader className="pb-2">
              <div className="flex flex-col gap-2 space-y-1">
                <CardTitle>Welcome Back!</CardTitle>
                <span className="text-wrap text-sm text-muted-foreground">
                  Sign in to an existing account with your email and password.
                </span>
                <span className="text-wrap text-sm text-muted-foreground">
                  Need an account?{" "}
                  <TabsList className="inline-block p-0 m-0 bg-transparent hover:underline">
                    <TabsTrigger
                      className="p-0 m-0 hover:underline font-semibold"
                      value="sign-up"
                    >
                      Sign up
                    </TabsTrigger>
                  </TabsList>
                </span>
              </div>
            </CardHeader>
            <CardContent className="space-y-1">
              <SignInForm />
            </CardContent>
          </Card>
        </TabsContent>
        <TabsContent value="sign-up">
          <Card>
            <CardHeader className="pb-2">
              <div className="flex flex-col gap-2 space-y-1">
                <CardTitle>Join us!</CardTitle>
                <span className="text-wrap text-sm text-muted-foreground">
                  Register a new account.
                  <br />
                  <br />
                </span>
                <span className="text-wrap text-sm text-muted-foreground">
                  Already have an account?{" "}
                  <TabsList className="inline-block p-0 m-0 bg-transparent hover:underline">
                    <TabsTrigger
                      className="p-0 m-0 hover:underline font-semibold"
                      value="sign-in"
                    >
                      Sign in
                    </TabsTrigger>
                  </TabsList>
                </span>
              </div>
            </CardHeader>
            <CardContent className="space-y-2">
              <SignUpForm />
            </CardContent>
          </Card>
        </TabsContent>
      </Tabs>
    </Suspense>
  );
}
