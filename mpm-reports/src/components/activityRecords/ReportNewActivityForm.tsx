"use client";
import { Button } from "@/components/ui/button";
import { Calendar } from "@/components/ui/calendar";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { createActivityRecordWithRelationsAction } from "@/lib/actions/activityRecords";
import {
  NewActivityRecordParams,
  deliveryMethodEnum,
  insertActivityRecordSchema,
  locationEnum,
} from "@/lib/db/schema/activityRecords";
import { User } from "@/lib/db/schema/users";
import { calculateFiscalQuarter, cn } from "@/lib/utils";
import { zodResolver } from "@hookform/resolvers/zod";
import { CalendarIcon } from "@radix-ui/react-icons";
import { format } from "date-fns";
import { useRouter } from "next/navigation";
import { HTMLInputTypeAttribute } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { UpdateTagParams as Tag } from "@/lib/db/schema/tags";
import MultiSelect from "../ui/multi-select";
import { toast } from "sonner";
import { ActivitiesWithRelations } from "@/lib/api/activities/queries";

interface ReportActivityProps {
  reportingUser: User;
  activitiesOptions: ActivitiesWithRelations[];
  otherPartiesOptions: User[];
  tagsOptions: Tag[];
}

export default function ReportNewActivityForm({
  reportingUser,
  activitiesOptions,
  otherPartiesOptions,
  tagsOptions,
}: ReportActivityProps) {
  // TODO: depending on dropdown selected Activity, show/hide literature doi field, if delivered offsite - where? (e.g. conference name and location), etc.
  const router = useRouter();

  // define schema
  const formSchema = insertActivityRecordSchema
    .omit({
      datePerformedEnd: true,
      datePerformedStart: true,
      quarter: true,
      reportingUserId: true,
    })
    .extend({
      date: z
        .object({
          from: z
            .date()
            .min(new Date(2015, 0, 1), {
              message: "Starting date must be after 2015",
            })
            .max(new Date(), {
              message: "Starting date cannot be in the future",
            }),
          to: z
            .date()
            .min(new Date(2015, 0, 1), {
              message: "Ending date must be after 2015",
            })
            .max(new Date(), {
              message: "Ending date cannot be in the future",
            }),
        })
        .refine((data) => data.from <= data.to, {
          message: "Ending date should occur on or after starting date",
        }),
      otherParties: z.array(
        z.object({
          value: z.string().min(1, {
            message: "otherParties value must be at least 1 character",
          }),
          label: z.string().min(1, {
            message: "otherParties label must be at least 1 character",
          }),
        }),
      ),
      tags: z.array(
        z.object({
          value: z
            .string()
            .min(1, { message: "Tag value must be at least 1 character" }),
          label: z
            .string()
            .min(1, { message: "Tag label must be at least 1 character" }),
        }),
      ),
      // .refine((value) => value.some((item) => item), {
      //   message: "You have to select at least one tag.",
      // }),
    });

  // Define your form
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      // datePerformedStart: new Date(), //? removed to parse separately
      // datePerformedEnd: new Date(), //? removed to parse separately
      // quarter: "FY24Q2", //? removed to parse separately
      activityId: "",
      description: "",
      departmentId: "",
      deliveryMethod: "not_tracked",
      location: "not_tracked",
      quantity: 1,
      otherParties: [],
      tags: [],
    },
  });

  // Define form fields
  type TextFormFields = {
    id: number;
    name: Extract<keyof NewActivityRecordParams, "description" | "quantity">;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    description: string;
    autocomplete: AutoFillNormalField | "off";
    options?: string[];
  }[];
  const textFormFields: TextFormFields = [
    {
      id: 1,
      name: "description",
      label: "Description",
      type: "text",
      placeholder: "",
      description: "A description of the activity performed",
      autocomplete: "off",
    },
    {
      id: 2,
      name: "quantity",
      label: "Quantity",
      type: "number",
      placeholder: "",
      description: "How many units were produced, attended, etc.",
      autocomplete: "off",
    },
  ];

  type RadioFormFields = {
    id: number;
    name: Extract<keyof NewActivityRecordParams, "location" | "deliveryMethod">;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    description: string;
    autocomplete: AutoFillNormalField | "off";
    options?: string[];
  }[];
  const radioFormFields: RadioFormFields = [
    {
      id: 3,
      name: "deliveryMethod",
      label: "Delivery Method",
      type: "radio",
      placeholder: "",
      description: "How the activity was delivered",
      autocomplete: "off",
      options: deliveryMethodEnum.enumValues,
    },
    {
      id: 4,
      name: "location",
      label: "Location",
      type: "radio",
      placeholder: "",
      description: "Where the activity was delivered",
      autocomplete: "off",
      options: locationEnum.enumValues,
    },
  ];

  // Define a submit handler
  const triggerAction = async (values: z.infer<typeof formSchema>) => {
    const { date, ...rest1 } = values;
    const { tags, ...rest2 } = rest1;
    const { otherParties, ...rest } = rest2;
    const { from, to } = date;
    const parsedValues: NewActivityRecordParams = {
      ...rest,
      reportingUserId: reportingUser.id,
      datePerformedStart: from,
      datePerformedEnd: to,
      quarter: await calculateFiscalQuarter(to),
    };
    const error = await createActivityRecordWithRelationsAction({
      activityRecord: parsedValues,
      tags: tags.map((tag) => ({ id: tag.value, tag: tag.label })),
      otherParties: otherParties.map((otherParty) => otherParty.value),
    });
    if (!error) {
      toast.success("New activity recorded successfully!");
      router.push("/activity-records");
    } else {
      toast.error("Error: " + error);
    }
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(triggerAction)}
        className="space-y-6 max-w-[800px] mx-auto"
      >
        {/* ACTIVITY */}
        <FormField
          control={form.control}
          name="activityId"
          render={({ field }) => (
            <FormItem>
              <FormLabel>
                <legend>Activity</legend>
              </FormLabel>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                  <SelectTrigger>
                    <SelectValue placeholder="Select an activity" />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {activitiesOptions?.map((activity) => (
                    <SelectItem key={activity.id} value={activity.id}>
                      {activity.name}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              {/* <FormDescription>
                                You can propose new Activity options {" "}
                                <Link href="/new-activity">here</Link>.
                            </FormDescription> */}
              <FormMessage />
            </FormItem>
          )}
        />
        {/* DEPARTMENT */}
        <FormField
          control={form.control}
          name="departmentId"
          render={({ field }) => (
            <FormItem>
              <FormLabel>
                <legend>Department</legend>
              </FormLabel>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl>
                  <SelectTrigger>
                    <SelectValue placeholder="Select a department" />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {activitiesOptions
                    .filter(
                      (activity) =>
                        activity.id === form.getValues("activityId"),
                    )[0]
                    ?.activitiesToDepartments.map(({ department }) => (
                      <SelectItem key={department.id} value={department.id}>
                        {department.name.charAt(0).toUpperCase() +
                          department.name.slice(1).toLowerCase()}
                      </SelectItem>
                    ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        {/* OTHER PARTIES */}
        <FormField
          control={form.control}
          name="otherParties"
          render={({ field }) => (
            <FormItem>
              <FormLabel asChild>
                <legend>Other Party Member(s)</legend>
              </FormLabel>
              <MultiSelect
                value={field.value}
                onChange={field.onChange}
                defaultOptions={otherPartiesOptions
                  .map((otherParty) => ({
                    value: otherParty.id,
                    label: `${otherParty.firstName} ${otherParty.lastName}`,
                  }))
                  .filter(
                    (otherParty) => otherParty.value !== reportingUser.id,
                  )}
                placeholder="Attach another party to activity..."
                hidePlaceholderWhenOptionsExhausted
                emptyIndicator={
                  <div className="flex flex-row items-center justify-center space-x-2 leading-10 text-gray-600 dark:text-gray-400">
                    <span
                      className="text-lg"
                      role="img"
                      aria-label="A person deep in thought"
                    >
                      🤔
                    </span>
                    <p className="text-sm">No more people found.</p>
                  </div>
                }
              />
              <FormMessage />
            </FormItem>
          )}
        />
        {/* ACTIVITY RECORD TEXT FIELDS */}
        {textFormFields.map((textFormField, i) => (
          <div
            key={i}
            className={`${textFormField.name === "quantity" ? "flex space-x-4" : ""}`}
          >
            <div className="w-full">
              <FormField
                key={textFormField.id}
                control={form.control}
                name={textFormField.name}
                defaultValue={textFormField.type}
                render={({ field }) => (
                  <FormItem>
                    <FormLabel asChild>
                      <legend>{textFormField.label}</legend>
                    </FormLabel>
                    <FormControl id={textFormField.name}>
                      {(textFormField.type === "text" ||
                        textFormField.type === "number") &&
                        (textFormField.name === "quantity" ? (
                          <Input
                            type={textFormField.type}
                            autoComplete={textFormField.autocomplete}
                            {...field}
                            value={field.value ?? ""}
                            onChange={(e) => {
                              if (e.target.value === "")
                                return field.onChange(undefined);
                              textFormField.type === "number"
                                ? field.onChange(Number(e.target.value))
                                : field.onChange(e.target.value);
                            }}
                          />
                        ) : (
                          <Input
                            type={textFormField.type}
                            autoComplete={textFormField.autocomplete}
                            {...field}
                            value={field.value ?? ""}
                            onChange={(e) => {
                              if (e.target.value === "")
                                return field.onChange(undefined);
                              textFormField.type === "number"
                                ? field.onChange(Number(e.target.value))
                                : field.onChange(e.target.value);
                            }}
                          />
                        ))}
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            {textFormField.name === "quantity" && (
              <div className="w-1/4">
                <FormField
                  control={form.control}
                  name="unitId"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>
                        <legend>Unit</legend>
                      </FormLabel>
                      <Select
                        onValueChange={field.onChange}
                        defaultValue={field.value}
                      >
                        <FormControl>
                          <SelectTrigger>
                            <SelectValue placeholder="Select a unit" />
                          </SelectTrigger>
                        </FormControl>
                        <SelectContent>
                          {form.getValues("activityId") === "" && (
                            <span className="flex w-full cursor-default select-none items-center rounded-sm py-1.5 pl-2 pr-2 text-sm outline-none focus:bg-accent focus:text-accent-foreground data-[disabled]:pointer-events-none data-[disabled]:opacity-50">
                              Please choose an activity first
                            </span>
                          )}
                          {activitiesOptions
                            .filter(
                              (activity) =>
                                activity.id === form.watch("activityId"),
                            )[0]
                            ?.activitiesToQuantities.map(({ quantity }) => (
                              <SelectItem key={quantity.id} value={quantity.id}>
                                {quantity.unit}
                              </SelectItem>
                            ))}
                          {activitiesOptions.filter(
                            (activity) =>
                              activity.id === form.getValues("activityId"),
                          )[0]?.activitiesToQuantities.length === 0 && (
                            <span className="flex w-full cursor-default select-none items-center rounded-sm py-1.5 pl-2 pr-2 text-sm outline-none focus:bg-accent focus:text-accent-foreground data-[disabled]:pointer-events-none data-[disabled]:opacity-50">
                              No unit options found
                            </span>
                          )}
                        </SelectContent>
                      </Select>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            )}
          </div>
        ))}
        {/* ACTIVITY RECORD RADIO FIELDS */}
        <div className="flex justify-left space-x-16">
          {radioFormFields.map((radioFormField) => {
            const activityInfo = activitiesOptions.filter(
              (activity) => activity.id === form.watch("activityId"),
            )[0];
            if (
              !activityInfo?.locationImportant &&
              radioFormField.name === "location"
            )
              return null;
            if (
              !activityInfo?.deliveryMethodImportant &&
              radioFormField.name === "deliveryMethod"
            )
              return null;
            return (
              <FormField
                key={radioFormField.id}
                control={form.control}
                name={radioFormField.name}
                render={({ field }) => (
                  <FormItem>
                    <FormLabel asChild>
                      <fieldset>{radioFormField.label}</fieldset>
                    </FormLabel>
                    <FormControl id={radioFormField.name}>
                      <>
                        {radioFormField.type === "radio" && (
                          <RadioGroup
                            name={radioFormField.name}
                            onValueChange={field.onChange}
                            defaultValue={"not_tracked"}
                            className="flex flex-col space-y-1"
                          >
                            {radioFormField.options?.map((option, i) => (
                              <FormItem
                                key={option}
                                className="flex items-center space-x-3 space-y-0"
                              >
                                <FormControl>
                                  <RadioGroupItem value={`${option}`} />
                                </FormControl>
                                <FormLabel className="font-normal">
                                  {option
                                    .replaceAll("_", " ")
                                    .split(" ")
                                    .map(
                                      (word) =>
                                        word.charAt(0).toUpperCase() +
                                        word.slice(1).toLowerCase(),
                                    )
                                    .join(" ")}
                                </FormLabel>
                              </FormItem>
                            ))}
                          </RadioGroup>
                        )}
                      </>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            );
          })}
        </div>
        {/* DATE */}
        <FormField
          control={form.control}
          name="date"
          render={({ field }) => (
            <FormItem>
              <FormLabel asChild>
                <legend>Date Range</legend>
              </FormLabel>
              <FormControl>
                <Popover>
                  <PopoverTrigger asChild>
                    <Button
                      variant={"outline"}
                      className={cn(
                        "w-full justify-start text-left font-normal",
                        !field.value && "text-muted-foreground",
                      )}
                    >
                      <CalendarIcon className="mr-2 h-4 w-4" />
                      {field.value?.from ? (
                        field.value.to ? (
                          <>
                            {format(field.value.from, "LLL dd, y")} -{" "}
                            {format(field.value.to, "LLL dd, y")}
                          </>
                        ) : (
                          format(field.value.from, "LLL dd, y")
                        )
                      ) : (
                        <span>Select starting and ending dates</span>
                      )}
                    </Button>
                  </PopoverTrigger>
                  <PopoverContent className="w-auto p-0" align="start">
                    <Calendar
                      initialFocus
                      mode="range"
                      defaultMonth={field.value?.from}
                      selected={field.value}
                      onSelect={field.onChange}
                      numberOfMonths={2}
                    />
                  </PopoverContent>
                </Popover>
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {/* TAGS */}
        <FormField
          control={form.control}
          name="tags"
          render={({ field }) => (
            <FormItem className="flex flex-col items-start">
              <FormLabel className="text-left" asChild>
                <legend>Tags</legend>
              </FormLabel>
              <FormControl>
                <MultiSelect
                  creatable
                  value={field.value}
                  onChange={field.onChange}
                  defaultOptions={tagsOptions.map((tag) => ({
                    value: tag.id,
                    label: tag.tag,
                  }))}
                  placeholder="Select a tag or enter a new one..."
                  emptyIndicator={
                    <div className="flex flex-row items-center justify-center space-x-2 leading-10 text-gray-600 dark:text-gray-400">
                      <span
                        className="text-lg"
                        role="img"
                        aria-label="A person deep in thought"
                      >
                        🤔
                      </span>
                      <p className="text-sm">
                        No additional tags found. Try adding a new one!
                      </p>
                    </div>
                  }
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        {/* SUBMIT */}
        <Button type="submit">Submit</Button>
      </form>
    </Form>
  );
}
