"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { insertActivityParams } from "@/lib/db/schema/activities";
import { HTMLInputTypeAttribute } from "react";
import { Input } from "../ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "../ui/select";
import { Checkbox } from "../ui/checkbox";
import { Department } from "@/lib/db/schema/departments";
import { Quantity } from "@/lib/db/schema/quantities";
import MultiSelect from "../ui/multi-select";
import { toast } from "sonner";
import { createActivityWithRelationsAction } from "@/lib/actions/activities";
import { ActivityGroupEnum, CodesEnum } from "@/lib/db/schema/activities";

type NewActivityFormProps = {
  departmentOptions: Department[];
  quantityOptions: Quantity[];
};

const formSchema = insertActivityParams.extend({
  quantities: z
    .array(
      z.object({
        value: z
          .string()
          .min(1, { message: "Quantity value must be at least 1 character" }),
        label: z
          .string()
          .min(1, { message: "Quantity label must be at least 1 character" }),
      }),
    )
    .refine((value) => value.some((item) => item), {
      message: "You have to select at least one quantity.",
    }),
  departments: z
    .array(
      z.object({
        value: z
          .string()
          .min(1, { message: "Department value must be at least 1 character" }),
        label: z
          .string()
          .min(1, { message: "Department label must be at least 1 character" }),
      }),
    )
    .refine((value) => value.some((item) => item), {
      message: "You have to select at least one department.",
    }),
});

export default function NewActivityForm({
  departmentOptions,
  quantityOptions,
}: NewActivityFormProps) {
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      quantities: [],
      departments: [],
      activityGroup: undefined,
      codes: undefined,
      locationImportant: false,
      deliveryMethodImportant: false,
    },
  });

  // 2. Define a submit handler.
  const triggerAction = async (values: z.infer<typeof formSchema>) => {
    const { departments, ...rest1 } = values;
    const { quantities, ...rest } = rest1;

    const error = await createActivityWithRelationsAction({
      activity: rest,
      quantities: quantities.map((quantity) => quantity.value),
      departments: departments.map((department) => department.value),
    });
    if (!error) {
      toast.success("New activity recorded successfully!");
    } else {
      toast.error("Error: " + error);
    }
  };

  // Define form fields
  type TextFormFields = {
    id: number;
    name: Extract<keyof z.infer<typeof formSchema>, "name">;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    description: string;
    autocomplete: AutoFillNormalField | "off";
    options?: string[];
  }[];
  const textFormFields: TextFormFields = [
    {
      id: 1,
      name: "name",
      label: "Activity Name",
      type: "text",
      placeholder: "",
      description: "A name for the new activity",
      autocomplete: "off",
    },
  ];

  type SelectFormFields = {
    id: number;
    name: Extract<keyof z.infer<typeof formSchema>, "codes" | "activityGroup">;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    description: string;
    autocomplete: AutoFillNormalField | "off";
    options?: string[];
  }[];
  const selectFormFields: SelectFormFields = [
    {
      id: 2,
      name: "codes",
      label: "CODES",
      type: "text",
      placeholder: "Select a CODES designation...",
      description: "CODES designation for the new activity",
      autocomplete: "off",
      options: Object.values(CodesEnum),
    },
    {
      id: 3,
      name: "activityGroup",
      label: "Group",
      type: "text",
      placeholder: "Select a group...",
      description: "Group designation for the new activity",
      autocomplete: "off",
      options: Object.values(ActivityGroupEnum),
    },
  ];

  type BooleanFormFields = {
    id: number;
    name: Extract<
      keyof z.infer<typeof formSchema>,
      "locationImportant" | "deliveryMethodImportant"
    >;
    label: string;
    type: HTMLInputTypeAttribute;
    placeholder: string;
    description: string;
    autocomplete: AutoFillNormalField | "off";
    options?: string[];
  }[];
  const booleanFormFields: BooleanFormFields = [
    {
      id: 4,
      name: "locationImportant",
      label: "Location Important",
      type: "checkbox",
      placeholder: "",
      description:
        "Is the location where the activity was performed important?",
      autocomplete: "off",
    },
    {
      id: 5,
      name: "deliveryMethodImportant",
      label: "Delivery Method Important",
      type: "checkbox",
      placeholder: "",
      description: "Is the delivery method of the activity important?",
      autocomplete: "off",
    },
  ];

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(triggerAction)} className="space-y-8">
        {/* DEPARTMENT DROPDOWN */}
        <FormField
          control={form.control}
          name="departments"
          render={({ field }) => (
            <FormItem>
              <FormLabel asChild>
                <legend>Assigned Department</legend>
              </FormLabel>
              <MultiSelect
                value={field.value}
                onChange={field.onChange}
                defaultOptions={departmentOptions.map((department) => ({
                  value: department.id,
                  label: department.name.toLocaleUpperCase(),
                }))}
                placeholder="Assign department(s)..."
                hidePlaceholderWhenOptionsExhausted
                emptyIndicator={
                  <div className="flex flex-row items-center justify-center space-x-2 leading-10 text-gray-600 dark:text-gray-400">
                    <span
                      className="text-lg"
                      role="img"
                      aria-label="A person deep in thought"
                    >
                      🤔
                    </span>
                    <p className="text-sm">No more departments found.</p>
                  </div>
                }
              />
              <FormMessage />
            </FormItem>
          )}
        />
        {/* ACTIVITY TEXT FIELDS */}
        {textFormFields.map((textFormField) => (
          <FormField
            key={textFormField.id}
            control={form.control}
            name={textFormField.name}
            defaultValue={textFormField.placeholder}
            render={({ field }) => (
              <FormItem>
                <FormLabel asChild>
                  <legend>{textFormField.label}</legend>
                </FormLabel>
                <FormControl id={textFormField.name}>
                  {textFormField.type === "text" && (
                    <div className="flex items-center justify-left space-x-4">
                      <Input
                        className="w-full"
                        type={textFormField.type}
                        autoComplete={textFormField.autocomplete}
                        {...field}
                        value={field.value ?? ""}
                      />
                    </div>
                  )}
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        {/* ACTIVITY SELECT FIELDS */}
        {selectFormFields.map((selectFormField) => (
          <FormField
            key={selectFormField.id}
            control={form.control}
            name={selectFormField.name}
            render={({ field }) => (
              <FormItem>
                <FormLabel>
                  <legend>{selectFormField.label}</legend>
                </FormLabel>
                <Select
                  onValueChange={field.onChange}
                  defaultValue={field.value}
                >
                  <FormControl>
                    <SelectTrigger>
                      <SelectValue placeholder={selectFormField.placeholder} />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    {selectFormField.options?.map((option, index) => (
                      <SelectItem
                        key={`${selectFormField.name}_${index}`}
                        value={option}
                      >
                        {option
                          .split(/\s+/)
                          .map(
                            (subword) =>
                              subword.charAt(0).toUpperCase() +
                              subword.slice(1),
                          )
                          .join(" ")}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        {/* QUANTITIES DROPDOWN */}
        <div className="flex items-end justify-left space-x-4">
          <div className="w-full">
            <FormField
              control={form.control}
              name="quantities"
              render={({ field }) => (
                <FormItem>
                  <FormLabel asChild>
                    <legend>Reporting Units</legend>
                  </FormLabel>
                  <MultiSelect
                    value={field.value}
                    onChange={field.onChange}
                    defaultOptions={quantityOptions.map((quantity) => ({
                      value: quantity.id,
                      label: quantity.unit.toLocaleUpperCase(),
                    }))}
                    placeholder="Select quantities..."
                    hidePlaceholderWhenOptionsExhausted
                    emptyIndicator={
                      <div className="flex flex-row items-center justify-center space-x-2 leading-10 text-gray-600 dark:text-gray-400">
                        <span
                          className="text-lg"
                          role="img"
                          aria-label="A person deep in thought"
                        >
                          🤔
                        </span>
                        <p className="text-sm">No more quantities found.</p>
                      </div>
                    }
                  />
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
        </div>
        {/* ACTIVITY BOOLEAN FIELDS */}
        <div className="flex space-x-4 justify-left">
          {booleanFormFields.map((booleanFormField) => (
            <FormField
              key={booleanFormField.id}
              control={form.control}
              name={booleanFormField.name}
              render={({ field }) => (
                <FormItem className="flex flex-row items-start space-x-3 space-y-0 rounded-md p-4 shadow">
                  <FormControl>
                    <Checkbox
                      checked={field.value}
                      onCheckedChange={field.onChange}
                    />
                  </FormControl>
                  <FormLabel>
                    <legend>{booleanFormField.label}</legend>
                  </FormLabel>
                  <FormMessage />
                </FormItem>
              )}
            />
          ))}
        </div>
        <Button type="submit">Submit</Button>
      </form>
    </Form>
  );
}
