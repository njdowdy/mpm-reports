"use client";

import { useState } from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";

import { cn } from "@/lib/utils";
import { type Activity, CompleteActivity } from "@/lib/db/schema/activities";
import Modal from "@/components/shared/Modal";
import {
  type ActivityRecord,
  type ActivityRecordId,
} from "@/lib/db/schema/activityRecords";
// import { useOptimisticActivities } from "@/app/(app)/activities/useOptimisticActivities";
import { Button } from "@/components/ui/button";
import ActivityForm from "./ActivityForm";
import { PlusIcon } from "lucide-react";

type TOpenModal = (activity?: Activity) => void;

export default function ActivityList({
  activities,
  activityRecords,
  activityRecordId,
}: {
  activities: CompleteActivity[];
  activityRecords: ActivityRecord[];
  activityRecordId?: ActivityRecordId;
}) {
  // const { optimisticActivities, addOptimisticActivity } =
  //   useOptimisticActivities(activities, activityRecords);
  const [open, setOpen] = useState(false);
  const [activeActivity, setActiveActivity] = useState<Activity | null>(null);
  const openModal = (activity?: Activity) => {
    setOpen(true);
    activity ? setActiveActivity(activity) : setActiveActivity(null);
  };
  const closeModal = () => setOpen(false);

  return (
    <div>
      <Modal
        open={open}
        setOpen={setOpen}
        title={activeActivity ? "Edit Activity" : "Create Activity"}
      >
        <ActivityForm
          activity={activeActivity}
          // addOptimistic={addOptimisticActivity}
          openModal={openModal}
          closeModal={closeModal}
          activityRecords={activityRecords}
          activityRecordId={activityRecordId}
        />
      </Modal>
      <div className="absolute right-0 top-0 ">
        <Button onClick={() => openModal()} variant={"outline"}>
          +
        </Button>
      </div>
      {/* {optimisticActivities.length === 0 ? (
        <EmptyState openModal={openModal} />
      ) : (
        <ul>
          {optimisticActivities.map((activity) => (
            <Activity
              activity={activity}
              key={activity.id}
              openModal={openModal}
            />
          ))}
        </ul>
      )} */}
    </div>
  );
}

const Activity = ({
  activity,
  openModal,
}: {
  activity: CompleteActivity;
  openModal: TOpenModal;
}) => {
  const optimistic = activity.id === "optimistic";
  const deleting = activity.id === "delete";
  const mutating = optimistic || deleting;
  const pathname = usePathname();
  const basePath = pathname.includes("activities")
    ? pathname
    : pathname + "/activities/";

  return (
    <li
      className={cn(
        "flex justify-between my-2",
        mutating ? "opacity-30 animate-pulse" : "",
        deleting ? "text-destructive" : "",
      )}
    >
      <div className="w-full">
        <div>{activity.name}</div>
      </div>
      <Button variant={"link"} asChild>
        <Link href={basePath + "/" + activity.id}>Edit</Link>
      </Button>
    </li>
  );
};

const EmptyState = ({ openModal }: { openModal: TOpenModal }) => {
  return (
    <div className="text-center">
      <h3 className="mt-2 text-sm font-semibold text-secondary-foreground">
        No activities
      </h3>
      <p className="mt-1 text-sm text-muted-foreground">
        Get started by creating a new activity.
      </p>
      <div className="mt-6">
        <Button onClick={() => openModal()}>
          <PlusIcon className="h-4" /> New Activities{" "}
        </Button>
      </div>
    </div>
  );
};
