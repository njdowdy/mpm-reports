import { z } from "zod";

import { useState, useTransition } from "react";
import { useFormStatus } from "react-dom";
import { useRouter } from "next/navigation";
import { toast } from "sonner";
import { useValidatedForm } from "@/lib/hooks/useValidatedForm";

import { type Action, cn, OptimisticAction } from "@/lib/utils";
// import { type TAddOptimistic } from "@/app/(app)/activities/useOptimisticActivities";

import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { useBackPath } from "@/components/shared/BackButton";

import { Checkbox } from "@/components/ui/checkbox";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

import {
  type Activity,
  insertActivityParams,
} from "@/lib/db/schema/activities";
import {
  createActivityAction,
  deleteActivityAction,
  updateActivityAction,
} from "@/lib/actions/activities";
import {
  type ActivityRecord,
  type ActivityRecordId,
} from "@/lib/db/schema/activityRecords";

export type TAddOptimistic = (action: OptimisticAction<Activity>) => void; //! added to remove error since "@/app/(app)/activities/useOptimisticActivities" was deleted for now

const ActivityForm = ({
  activityRecords,
  activityRecordId,
  activity,
  openModal,
  closeModal,
  addOptimistic,
  postSuccess,
}: {
  activity?: Activity | null;
  activityRecords: ActivityRecord[];
  activityRecordId?: ActivityRecordId;
  openModal?: (activity?: Activity) => void;
  closeModal?: () => void;
  addOptimistic?: TAddOptimistic;
  postSuccess?: () => void;
}) => {
  const { errors, hasErrors, setErrors, handleChange } =
    useValidatedForm<Activity>(insertActivityParams);
  const editing = !!activity?.id;

  const [isDeleting, setIsDeleting] = useState(false);
  const [pending, startMutation] = useTransition();

  const router = useRouter();
  const backpath = useBackPath("activities");

  const onSuccess = (
    action: Action,
    data?: { error: string; values: Activity },
  ) => {
    const failed = Boolean(data?.error);
    if (failed) {
      openModal && openModal(data?.values);
      toast.error(`Failed to ${action}`, {
        description: data?.error ?? "Error",
      });
    } else {
      router.refresh();
      postSuccess && postSuccess();
      toast.success(`Activity ${action}d!`);
      if (action === "delete") router.push(backpath);
    }
  };

  const handleSubmit = async (data: FormData) => {
    setErrors(null);

    const payload = Object.fromEntries(data.entries());
    const activityParsed = await insertActivityParams.safeParseAsync({
      activityRecordId,
      ...payload,
    });
    if (!activityParsed.success) {
      setErrors(activityParsed?.error.flatten().fieldErrors);
      return;
    }

    closeModal && closeModal();
    const values = activityParsed.data;
    const pendingActivity: Activity = {
      updatedAt: activity?.updatedAt ?? new Date(),
      createdAt: activity?.createdAt ?? new Date(),
      id: activity?.id ?? "",
      ...values,
    };
    try {
      startMutation(async () => {
        addOptimistic &&
          addOptimistic({
            data: pendingActivity,
            action: editing ? "update" : "create",
          });

        const error = editing
          ? await updateActivityAction({ ...values, id: activity.id })
          : await createActivityAction(values);

        const errorFormatted = {
          error: error ?? "Error",
          values: pendingActivity,
        };
        onSuccess(
          editing ? "update" : "create",
          error ? errorFormatted : undefined,
        );
      });
    } catch (e) {
      if (e instanceof z.ZodError) {
        setErrors(e.flatten().fieldErrors);
      }
    }
  };

  return (
    <form action={handleSubmit} onChange={handleChange} className={"space-y-8"}>
      {/* Schema fields start */}
      <div>
        <Label
          className={cn(
            "mb-2 inline-block",
            errors?.name ? "text-destructive" : "",
          )}
        >
          Name
        </Label>
        <Input
          type="text"
          name="name"
          className={cn(errors?.name ? "ring ring-destructive" : "")}
          defaultValue={activity?.name ?? ""}
        />
        {errors?.name ? (
          <p className="text-xs text-destructive mt-2">{errors.name[0]}</p>
        ) : (
          <div className="h-6" />
        )}
      </div>
      <div>
        <Label
          className={cn(
            "mb-2 inline-block",
            errors?.activityGroup ? "text-destructive" : "",
          )}
        >
          Group
        </Label>
        <Input
          type="text"
          name="activityGroup"
          className={cn(errors?.activityGroup ? "ring ring-destructive" : "")}
          defaultValue={activity?.activityGroup ?? ""}
        />
        {errors?.activityGroup ? (
          <p className="text-xs text-destructive mt-2">
            {errors.activityGroup[0]}
          </p>
        ) : (
          <div className="h-6" />
        )}
      </div>
      <div>
        <Label
          className={cn(
            "mb-2 inline-block",
            errors?.codes ? "text-destructive" : "",
          )}
        >
          Codes
        </Label>
        <Input
          type="text"
          name="codes"
          className={cn(errors?.codes ? "ring ring-destructive" : "")}
          defaultValue={activity?.codes ?? ""}
        />
        {errors?.codes ? (
          <p className="text-xs text-destructive mt-2">{errors.codes[0]}</p>
        ) : (
          <div className="h-6" />
        )}
      </div>
      <div>
        <Label
          className={cn(
            "mb-2 inline-block",
            errors?.locationImportant ? "text-destructive" : "",
          )}
        >
          Location Important
        </Label>
        <br />
        <Checkbox
          defaultChecked={activity?.locationImportant}
          name={"locationImportant"}
          className={cn(
            errors?.locationImportant ? "ring ring-destructive" : "",
          )}
        />
        {errors?.locationImportant ? (
          <p className="text-xs text-destructive mt-2">
            {errors.locationImportant[0]}
          </p>
        ) : (
          <div className="h-6" />
        )}
      </div>
      <div>
        <Label
          className={cn(
            "mb-2 inline-block",
            errors?.deliveryMethodImportant ? "text-destructive" : "",
          )}
        >
          Delivery Method Important
        </Label>
        <br />
        <Checkbox
          defaultChecked={activity?.deliveryMethodImportant}
          name={"deliveryMethodImportant"}
          className={cn(
            errors?.deliveryMethodImportant ? "ring ring-destructive" : "",
          )}
        />
        {errors?.deliveryMethodImportant ? (
          <p className="text-xs text-destructive mt-2">
            {errors.deliveryMethodImportant[0]}
          </p>
        ) : (
          <div className="h-6" />
        )}
      </div>
      {/* Schema fields end */}

      {/* Save Button */}
      <SaveButton errors={hasErrors} editing={editing} />

      {/* Delete Button */}
      {editing ? (
        <Button
          type="button"
          disabled={isDeleting || pending || hasErrors}
          variant={"destructive"}
          onClick={() => {
            setIsDeleting(true);
            closeModal && closeModal();
            startMutation(async () => {
              addOptimistic &&
                addOptimistic({ action: "delete", data: activity });
              const error = await deleteActivityAction(activity.id);
              setIsDeleting(false);
              const errorFormatted = {
                error: error ?? "Error",
                values: activity,
              };

              onSuccess("delete", error ? errorFormatted : undefined);
            });
          }}
        >
          Delet{isDeleting ? "ing..." : "e"}
        </Button>
      ) : null}
    </form>
  );
};

export default ActivityForm;

const SaveButton = ({
  editing,
  errors,
}: {
  editing: Boolean;
  errors: boolean;
}) => {
  const { pending } = useFormStatus();
  const isCreating = pending && editing === false;
  const isUpdating = pending && editing === true;
  return (
    <Button
      type="submit"
      className="mr-2"
      disabled={isCreating || isUpdating || errors}
      aria-disabled={isCreating || isUpdating || errors}
    >
      {editing
        ? `Sav${isUpdating ? "ing..." : "e"}`
        : `Creat${isCreating ? "ing..." : "e"}`}
    </Button>
  );
};
