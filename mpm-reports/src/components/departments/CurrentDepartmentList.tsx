"use client";
import { Suspense } from "react";
import Loading from "@/app/loading";
import { TrashIcon } from "lucide-react";
import { Department } from "@/lib/db/schema/departments";
import { Button } from "../ui/button";
import { removeUserToDepartmentAction } from "@/lib/actions/users";
import { useRouter } from "next/navigation";
import { toast } from "sonner";

type CurrentDepartmentListProps = {
  departmentList: Department[];
  isOwner?: boolean;
};

export default function CurrentDepartmentList({
  departmentList,
  isOwner = false,
}: CurrentDepartmentListProps) {
  const router = useRouter();

  async function handleClick(e: React.MouseEvent<HTMLButtonElement>) {
    const deptName = e.currentTarget.name;
    try {
      await removeUserToDepartmentAction(e.currentTarget.id);
      toast.success(
        `You have been removed from the ${deptName.toLocaleUpperCase()} department.`,
      );
      router.refresh();
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <Suspense fallback={<Loading />}>
      <div>
        <h3 className="text-2xl font-medium mb-2">
          Currently Assigned Department(s):
        </h3>
        <div className="flex justify-left">
          {departmentList === undefined || departmentList.length === 0 ? (
            <span>User is not currently assigned to any departments.</span>
          ) : (
            <ul className="text-lg space-y-1">
              {departmentList
                .sort((a, b) => (a.name > b.name ? 1 : -1))
                .map((department) => (
                  <li key={department.id}>
                    <div className="flex items-center">
                      {department.name.toLocaleUpperCase()}
                      {isOwner && (
                        <Button
                          size="icon"
                          className="ml-1 cursor-pointer h-4 w-4 bg-background text-destructive hover:bg-transparent"
                          id={department.id}
                          name={department.name}
                          onClick={handleClick}
                          type="button"
                        >
                          <TrashIcon className="h-4 w-4" />
                        </Button>
                      )}
                    </div>
                  </li>
                ))}
            </ul>
          )}
        </div>
      </div>
    </Suspense>
  );
}
