import DepartmentForm from "@/components/departments/DepartmentForm";
import { Suspense } from "react";
import Loading from "@/app/loading";
import { Department } from "@/lib/db/schema/departments";

type DepartmentRequestProps = {
  departmentList: Department[];
};

export default async function DepartmentRequest({
  departmentList,
}: DepartmentRequestProps) {
  return (
    <Suspense fallback={<Loading />}>
      <DepartmentForm departmentOptions={departmentList} />
    </Suspense>
  );
}
