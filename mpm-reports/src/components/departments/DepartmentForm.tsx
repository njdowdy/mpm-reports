"use client";

import {
  Department,
  departmentIdSchema,
  insertDepartmentSchema,
} from "@/lib/db/schema/departments";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { addUserToDepartmentAction } from "@/lib/actions/users";
import { useRouter } from "next/navigation";
import { toast } from "sonner";

const formSchema = departmentIdSchema;

type DepartmentFormProps = {
  departmentOptions: Department[];
};

export default function DepartmentForm({
  departmentOptions,
}: DepartmentFormProps) {
  const router = useRouter();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: undefined,
    },
  });

  // 2. Define a submit handler.
  //TODO: standardize to either arrow or function declaration; I don't know which is preferred yet
  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      if (values.id === "none") return;
      const deptName = departmentOptions.find(
        (dept) => dept.id === values.id,
      )?.name;
      if (deptName) {
        await addUserToDepartmentAction(values.id);
        toast.success(
          `You have been added to the ${deptName.toLocaleUpperCase()} department.`,
        );
        router.refresh();
      }
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="id"
          render={({ field }) => (
            <FormItem>
              <FormLabel asChild>
                <legend>
                  <h3 className="text-2xl font-medium">
                    {/* Request Departmental Assignment */}
                    Join a Department
                  </h3>
                </legend>
              </FormLabel>
              <FormControl>
                <div className="flex justify-left space-x-3">
                  <Select
                    onValueChange={field.onChange}
                    defaultValue={field.value}
                  >
                    <SelectTrigger className="w-1/2 mt-2">
                      <SelectValue placeholder="Choose a department..." />
                    </SelectTrigger>
                    <SelectContent>
                      {departmentOptions.length !== 0 ? (
                        departmentOptions.map((department) => (
                          <SelectItem key={department.id} value={department.id}>
                            {department.name.toLocaleUpperCase()}
                          </SelectItem>
                        ))
                      ) : (
                        <p className="flex w-full cursor-default select-none items-center rounded-sm py-1.5 pl-8 pr-2 text-sm outline-none focus:bg-accent focus:text-accent-foreground data-[disabled]:pointer-events-none data-[disabled]:opacity-50">
                          No other departments available.
                        </p>
                      )}
                    </SelectContent>
                  </Select>
                  <Button type="submit" size="sm" className="w-1/12 mt-2">
                    {/* Request */}
                    Join
                  </Button>
                </div>
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
      </form>
    </Form>
  );
}
