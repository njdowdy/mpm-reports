"use client";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { uploadPublicImage } from "@/lib/supabase/client";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { toast } from "sonner";
import { z } from "zod";
import { updateAvatarUrlAction } from "@/lib/actions/users";
import { useRouter } from "next/navigation";

const ACCEPTED_IMAGE_TYPES = ["image/png", "image/jpg", "image/jpeg"];
const MAX_IMAGE_SIZE = 40; //In MegaBytes

const sizeInMB = (sizeInBytes: number, decimalsNum = 2) => {
  const result = sizeInBytes / (1024 * 1024);
  return +result.toFixed(decimalsNum);
};

const formSchema = z.object({
  avatarImage: z
    .custom<FileList>()
    .refine((files) => {
      return Array.from(files ?? []).length !== 0;
    }, "Image is required")
    .refine((files) => {
      return Array.from(files ?? []).every(
        (file) => sizeInMB(file.size) <= MAX_IMAGE_SIZE,
      );
    }, `The maximum image size is ${MAX_IMAGE_SIZE}MB`)
    .refine((files) => {
      return Array.from(files ?? []).every((file) =>
        ACCEPTED_IMAGE_TYPES.includes(file.type),
      );
    }, "File type is not supported"),
});

export default function AvatarImageUpload() {
  const router = useRouter();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
  });

  const fileRef = form.register("avatarImage");

  //TODO: standardize to either arrow or function declaration; I don't know which is preferred yet
  const onSubmit = async (formData: z.infer<typeof formSchema>) => {
    const image = formData.avatarImage[0];
    // TODO: Use Cropper.js to crop to a square/circular portion of the image before uploading -- pop out into a modal
    const { data, error } = JSON.parse(
      await uploadPublicImage(image, "avatars"),
    );
    if (error) {
      toast.error(error.message);
      return;
    }
    toast.success(`Image uploaded successfully`);
    await updateAvatarUrlAction(data.path);
    router.refresh();
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <FormField
          control={form.control}
          name="avatarImage"
          render={({ field }) => {
            return (
              <FormItem>
                <FormLabel className="text-2xl font-medium">
                  Upload a Profile Image
                </FormLabel>
                <div className="flex items-center justify-left space-x-3">
                  <FormControl className="w-1/2">
                    <Input
                      type="file"
                      multiple={false}
                      placeholder=""
                      className="mt-2"
                      {...fileRef}
                    />
                  </FormControl>
                  <Button type="submit" size="sm" className="w-1/12 mt-2">
                    Upload
                  </Button>
                </div>
                <FormMessage />
              </FormItem>
            );
          }}
        />
      </form>
    </Form>
  );
}
