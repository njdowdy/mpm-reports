"use client";

import { Button } from "@/components/ui/button";
import { useRouter } from "next/navigation";
import { useCallback, useEffect, useRef } from "react";
import { FaWindowClose } from "react-icons/fa";

export interface ModalProps {
  children: React.ReactNode;
}

export default function ModalClose() {
  const router = useRouter();
  const closeModal = useCallback(() => {
    router.back();
  }, [router]);

  const buttonRef = useRef<HTMLButtonElement>(null);

  // Add event listener for "esc" key press
  useEffect(() => {
    const handleKeyPress = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        closeModal();
      }
    };
    if (buttonRef.current) {
      buttonRef.current.focus();
    }

    window.addEventListener("keydown", handleKeyPress);

    return () => {
      window.removeEventListener("keydown", handleKeyPress);
    };
  }, [closeModal]);

  return (
    <div className="absolute right-4 top-4">
      <Button
        variant="link"
        size="icon"
        onClick={closeModal}
        ref={buttonRef}
        className="h-5 w-5 rounded-none text-foreground hover:cursor-pointer"
      >
        <FaWindowClose className="h-5 w-5 " />
      </Button>
    </div>
  );
}
