"use server";

import { revalidatePath } from "next/cache";
import {
  createActivityRecord,
  createActivityRecordWithRelations,
  deleteActivityRecord,
  updateActivityRecord,
} from "@/lib/api/activityRecords/mutations";
import {
  ActivityRecordId,
  NewActivityRecordParams,
  UpdateActivityRecordParams,
  activityRecordIdSchema,
  insertActivityRecordParams,
  updateActivityRecordParams,
} from "@/lib/db/schema/activityRecords";
import { UserId } from "../db/schema/users";
import { NewTag } from "../db/schema/tags";

const handleErrors = (e: unknown) => {
  const errMsg = "Error, please try again.";
  if (e instanceof Error) return e.message.length > 0 ? e.message : errMsg;
  if (e && typeof e === "object" && "error" in e) {
    const errAsStr = e.error as string;
    return errAsStr.length > 0 ? errAsStr : errMsg;
  }
  return errMsg;
};

const revalidateActivityRecords = () => revalidatePath("/activity-records");

export const createActivityRecordAction = async (
  input: NewActivityRecordParams,
) => {
  try {
    const payload = insertActivityRecordParams.parse(input);
    await createActivityRecord(payload);
    revalidateActivityRecords();
  } catch (e) {
    return handleErrors(e);
  }
};

export const updateActivityRecordAction = async (
  input: UpdateActivityRecordParams,
) => {
  try {
    const payload = updateActivityRecordParams.parse(input);
    await updateActivityRecord(payload.id, payload);
    revalidateActivityRecords();
  } catch (e) {
    return handleErrors(e);
  }
};

export const deleteActivityRecordAction = async (input: ActivityRecordId) => {
  try {
    const payload = activityRecordIdSchema.parse({ id: input });
    await deleteActivityRecord(payload.id);
    revalidateActivityRecords();
  } catch (e) {
    return handleErrors(e);
  }
};

export const createActivityRecordWithRelationsAction = async ({
  activityRecord,
  tags,
  otherParties,
}: {
  activityRecord: NewActivityRecordParams;
  tags: NewTag[];
  otherParties: UserId[];
}) => {
  try {
    const payload = {
      activityRecord: insertActivityRecordParams.parse(activityRecord),
      tags,
      otherParties,
    };
    await createActivityRecordWithRelations(
      payload.activityRecord,
      payload.tags,
      payload.otherParties,
    );
    revalidateActivityRecords();
  } catch (e) {
    return handleErrors(e);
  }
};
