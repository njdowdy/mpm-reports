"use server";

import { revalidatePath } from "next/cache";
import {
  createActivity,
  createActivityWithRelations,
  deleteActivity,
  updateActivity,
} from "@/lib/api/activities/mutations";
import {
  ActivityId,
  NewActivityParams,
  UpdateActivityParams,
  activityIdSchema,
  insertActivityParams,
  updateActivityParams,
} from "@/lib/db/schema/activities";
import { QuantityId } from "@/lib/db/schema/quantities";
import { DepartmentId } from "@/lib/db/schema/departments";

const handleErrors = (e: unknown) => {
  const errMsg = "Error, please try again.";
  if (e instanceof Error) return e.message.length > 0 ? e.message : errMsg;
  if (e && typeof e === "object" && "error" in e) {
    const errAsStr = e.error as string;
    return errAsStr.length > 0 ? errAsStr : errMsg;
  }
  return errMsg;
};

const revalidateActivities = () => revalidatePath("/activities");

export const createActivityAction = async (input: NewActivityParams) => {
  try {
    const payload = insertActivityParams.parse(input);
    await createActivity(payload);
    revalidateActivities();
  } catch (e) {
    return handleErrors(e);
  }
};

export const updateActivityAction = async (input: UpdateActivityParams) => {
  try {
    const payload = updateActivityParams.parse(input);
    await updateActivity(payload.id, payload);
    revalidateActivities();
  } catch (e) {
    return handleErrors(e);
  }
};

export const deleteActivityAction = async (input: ActivityId) => {
  try {
    const payload = activityIdSchema.parse({ id: input });
    await deleteActivity(payload.id);
    revalidateActivities();
  } catch (e) {
    return handleErrors(e);
  }
};

export const createActivityWithRelationsAction = async ({
  activity,
  quantities,
  departments,
}: {
  activity: NewActivityParams;
  quantities: QuantityId[];
  departments: DepartmentId[];
}) => {
  try {
    const payload = {
      activity: insertActivityParams.parse(activity),
      quantities,
      departments,
    };
    await createActivityWithRelations(
      payload.activity,
      payload.quantities,
      payload.departments,
    );
    revalidateActivities();
  } catch (e) {
    return handleErrors(e);
  }
};
