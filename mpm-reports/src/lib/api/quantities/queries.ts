import { db } from "@/lib/db/index";
import {
  ActivityId,
  activitiesToQuantities,
  activityIdSchema,
} from "@/lib/db/schema/activities";
import { quantities, type QuantityId } from "@/lib/db/schema/quantities";
import { eq } from "drizzle-orm";

export const getQuantities = async () => {
  const rows = await db.select({ quantity: quantities }).from(quantities);
  const a = rows.map((r) => ({
    ...r.quantity,
  }));
  return { quantities: a };
};

export const getQuantitiesByActivityId = async (id: ActivityId) => {
  const { id: activityId } = activityIdSchema.parse({ id });
  const rows = await db
    .select({ quantity: quantities })
    .from(quantities)
    .leftJoin(
      activitiesToQuantities,
      eq(activitiesToQuantities.quantityId, quantities.id),
    )
    .where(eq(activitiesToQuantities.activityId, activityId));
  if (rows === undefined) return {};
  const u = rows;
  return { quantities: u };
};
