import { db } from "@/lib/db/index";
import { and, eq } from "drizzle-orm";
import {
  UserId,
  NewUserParams,
  UpdateUserParams,
  updateUserSchema,
  insertUserSchema,
  users,
  userIdSchema,
  usersToDepartments,
} from "@/lib/db/schema/users";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";

export const createUser = async (user: NewUserParams) => {
  const newUser = insertUserSchema.parse(user);
  try {
    const [u] = await db.insert(users).values(newUser).returning();
    return { user: u };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const updateUser = async (id: UserId, user: UpdateUserParams) => {
  const { id: userId } = userIdSchema.parse({ id });
  const newUser = updateUserSchema.parse(user);
  try {
    const [u] = await db
      .update(users)
      .set({ ...newUser, updatedAt: new Date() })
      .where(eq(users.id, userId!))
      .returning();
    return { user: u };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const deleteUser = async (id: UserId) => {
  const { id: userId } = userIdSchema.parse({ id });
  try {
    const [u] = await db.delete(users).where(eq(users.id, userId!)).returning();
    return { user: u };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const updateAvatarUrl = async (url: string) => {
  const user = await getCurrentUser();
  if (!user) return;
  try {
    const [u] = await db
      .update(users)
      .set({ avatarUrl: url, updatedAt: new Date() })
      .where(eq(users.id, user.id!))
      .returning();
    return { user: u };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const addUserToDepartment = async (department_id: string) => {
  const user = await getCurrentUser();
  if (!user) return;
  try {
    const [u] = await db
      .insert(usersToDepartments)
      .values({ userId: user.id, departmentId: department_id })
      .returning();
    return { user: u };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const removeUserToDepartment = async (department_id: string) => {
  const user = await getCurrentUser();
  if (!user) return;
  try {
    const [u] = await db
      .delete(usersToDepartments)
      .where(
        and(
          eq(usersToDepartments.userId, user.id),
          eq(usersToDepartments.departmentId, department_id),
        ),
      )
      .returning();
    return { deletedDepartmentId: u.departmentId };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};
