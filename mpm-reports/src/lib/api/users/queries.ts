import { Department } from "./../../db/schema/departments";
import { db, InferQueryModel } from "@/lib/db/index";
import { eq, inArray } from "drizzle-orm";
import {
  type UserId,
  userIdSchema,
  users,
  usersToDepartments,
  userUsernameSchema,
} from "@/lib/db/schema/users";
import { activityRecords } from "@/lib/db/schema/activityRecords";
import { getDepartmentByName } from "../departments/queries";
import { type DepartmentEnum } from "@/lib/db/schema/departments";

export const getUsers = async () => {
  const rows = await db.select().from(users);
  const u = rows;
  return { users: u };
};

export const getUserById = async (id: UserId) => {
  const { id: userId } = userIdSchema.parse({ id });
  const [row] = await db.select().from(users).where(eq(users.id, userId));
  if (row === undefined) return {};
  const u = row;
  return { user: u };
};

export const getUserByUsername = async (username: string) => {
  const { username: usernameParsed } = userUsernameSchema.parse({ username });
  const [row] = await db
    .select()
    .from(users)
    .where(eq(users.username, usernameParsed));
  if (row === undefined) return {};
  const u = row;
  return { user: u };
};

export const getUsersByDepartmentName = async (name: DepartmentEnum) => {
  const { department } = await getDepartmentByName(name);
  if (department === undefined) return {};
  const rows = await db
    .select()
    .from(users)
    .leftJoin(usersToDepartments, eq(users.id, usersToDepartments.userId))
    .where(eq(usersToDepartments.departmentId, department?.id));
  if (rows === undefined) return {};
  const u = rows;
  return { users: u };
};

export const getUserByIdWithActivityRecordsWithActivities = async (
  id: UserId,
) => {
  const { id: userId } = userIdSchema.parse({ id });
  const rows = await db.query.activityRecords.findMany({
    where: eq(activityRecords.reportingUserId, userId),
    with: {
      activity: true,
      otherPartiesToActivityRecords: {
        with: {
          otherParty: true,
        },
      },
    },
  });

  if (rows.length === 0) return {};
  return { rows };
};

export type UserWithActivityRecordsWithActivities = InferQueryModel<
  "activityRecords",
  {
    activityId: true;
    createdAt: true;
    datePerformedEnd: true;
    datePerformedStart: true;
    deliveryMethod: true;
    description: true;
    departmentId: true;
    unitId: true;
    id: true;
    location: true;
    quarter: true;
    quantity: true;
    reportingUserId: true;
    updatedAt: true;
  },
  {
    activity: true;
    otherPartiesToActivityRecords: { with: { otherParty: true } };
  }
>;

export const getUsersByDepartments = async (departments: Department[]) => {
  if (departments.length === 0) return {};

  const rows = await db
    .select({ user: users })
    .from(users)
    .leftJoin(usersToDepartments, eq(usersToDepartments.userId, users.id))
    .where(
      inArray(
        usersToDepartments.departmentId,
        departments.map((d) => d.id),
      ),
    );
  if (rows === undefined) return {};
  const u = rows;
  return { users: u };
};
