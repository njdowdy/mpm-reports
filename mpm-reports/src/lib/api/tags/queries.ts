import { db } from "@/lib/db/index";
import { eq } from "drizzle-orm";
import { type TagId, tagIdSchema, tags } from "@/lib/db/schema/tags";

export const getTags = async () => {
  const rows = await db.select({ tag: tags }).from(tags);
  const a = rows.map((r) => ({
    ...r.tag,
  }));
  return { tags: a };
};
