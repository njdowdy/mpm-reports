import { db } from "@/lib/db/index";
import { eq } from "drizzle-orm";
import {
  TagId,
  NewTagParams,
  UpdateTagParams,
  updateTagSchema,
  insertTagSchema,
  tags,
  tagIdSchema,
} from "@/lib/db/schema/tags";

export const createTag = async (tag: NewTagParams) => {
  const newTag = insertTagSchema.parse(tag);
  try {
    const [a] = await db.insert(tags).values(newTag).returning();
    return { tag: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const updateTag = async (id: TagId, tag: UpdateTagParams) => {
  const { id: tagId } = tagIdSchema.parse({ id });
  const newTag = updateTagSchema.parse(tag);
  try {
    const [a] = await db
      .update(tags)
      .set({ ...newTag, updatedAt: new Date() })
      .where(eq(tags.id, tagId!))
      .returning();
    return { tag: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const deleteTag = async (id: TagId) => {
  const { id: tagId } = tagIdSchema.parse({ id });
  try {
    const [a] = await db.delete(tags).where(eq(tags.id, tagId!)).returning();
    return { tag: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};
