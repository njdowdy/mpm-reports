import { db, InferQueryModel } from "@/lib/db/index";
import { count, eq } from "drizzle-orm";
import {
  type ActivityRecordId,
  activityRecordIdSchema,
  activityRecords,
} from "@/lib/db/schema/activityRecords";
import { users } from "@/lib/db/schema/users";

export const getActivityRecords = async () => {
  const rows = await db
    .select({ activityRecord: activityRecords, user: users })
    .from(activityRecords)
    .leftJoin(users, eq(activityRecords.reportingUserId, users.id));
  const a = rows.map((r) => ({ ...r.activityRecord, user: r.user }));
  return { activityRecords: a };
};

export const getQuarterlyActivitySummary = async () => {
  const rows = await db
    .select({
      quarter: activityRecords.quarter,
      activityRecordsCount: count(activityRecords.id),
    })
    .from(activityRecords)
    .groupBy(activityRecords.quarter)
    .orderBy(activityRecords.quarter);
  return { rows };
};

export type QuarterlyActivitySummary = Awaited<
  ReturnType<typeof getQuarterlyActivitySummary>
>["rows"][number];

export const getActivityRecordsByQuarterWithRelations = async (
  quarter: string,
) => {
  const rows = await db.query.activityRecords.findMany({
    where: eq(activityRecords.quarter, quarter),
    with: {
      activity: true,
      department: true,
      unit: true,
      reportingUser: {
        with: {
          usersToDepartments: {
            with: {
              department: true,
            },
          },
        },
      },
      tagsToActivityRecords: {
        with: {
          tag: true,
        },
      },
    },
  });
  if (rows.length === 0) return {};
  return { rows };
};

export const getActivityRecordsByIdWithRelations = async (
  activityRecordId: string,
) => {
  const rows = await db.query.activityRecords.findFirst({
    where: eq(activityRecords.id, activityRecordId),
    with: {
      activity: true,
      department: true,
      unit: true,
      reportingUser: {
        with: {
          usersToDepartments: {
            with: {
              department: true,
            },
          },
        },
      },
      tagsToActivityRecords: {
        with: {
          tag: true,
        },
      },
    },
  });
  if (!rows) return {};
  return { rows };
};

export type ActivityRecordsWithRelations = InferQueryModel<
  "activityRecords",
  {
    activityId: true;
    createdAt: true;
    datePerformedEnd: true;
    datePerformedStart: true;
    deliveryMethod: true;
    description: true;
    id: true;
    location: true;
    quarter: true;
    quantity: true;
    reportingUserId: true;
    updatedAt: true;
    unitId: true;
    departmentId: true;
  },
  {
    activity: true;
    unit: true;
    department: true;
    reportingUser: {
      with: {
        usersToDepartments: {
          with: {
            department: true;
          };
        };
      };
    };
    tagsToActivityRecords: {
      with: {
        tag: true;
      };
    };
  }
>;

export const getActivityRecordById = async (id: ActivityRecordId) => {
  const { id: activityRecordId } = activityRecordIdSchema.parse({ id });
  const [row] = await db
    .select({ activityRecord: activityRecords, user: users })
    .from(activityRecords)
    .where(eq(activityRecords.id, activityRecordId))
    .leftJoin(users, eq(activityRecords.reportingUserId, users.id));
  if (row === undefined) return {};
  const a = { ...row.activityRecord, user: row.user };
  return { activityRecord: a };
};
