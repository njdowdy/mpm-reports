import { db } from "@/lib/db/index";
import { eq } from "drizzle-orm";
import {
  ActivityRecordId,
  NewActivityRecordParams,
  UpdateActivityRecordParams,
  updateActivityRecordSchema,
  insertActivityRecordSchema,
  activityRecords,
  activityRecordIdSchema,
} from "@/lib/db/schema/activityRecords";
import {
  NewTag,
  tagsToActivityRecords,
  tags as tagsTable,
} from "@/lib/db/schema/tags";
import { UserId, otherPartiesToActivityRecords } from "@/lib/db/schema/users";
import { generate_uuid4 } from "@/lib/utils";

export const createActivityRecord = async (
  activityRecord: NewActivityRecordParams,
) => {
  const newActivityRecord = insertActivityRecordSchema.parse(activityRecord);
  try {
    const [a] = await db
      .insert(activityRecords)
      .values(newActivityRecord)
      .returning();
    return { activityRecord: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const updateActivityRecord = async (
  id: ActivityRecordId,
  activityRecord: UpdateActivityRecordParams,
) => {
  const { id: activityRecordId } = activityRecordIdSchema.parse({ id });
  const newActivityRecord = updateActivityRecordSchema.parse(activityRecord);
  try {
    const [a] = await db
      .update(activityRecords)
      .set({ ...newActivityRecord, updatedAt: new Date() })
      .where(eq(activityRecords.id, activityRecordId!))
      .returning();
    return { activityRecord: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const deleteActivityRecord = async (id: ActivityRecordId) => {
  const { id: activityRecordId } = activityRecordIdSchema.parse({ id });
  try {
    //? CASCADE delete effect is defined in table definition
    const [a] = await db
      .delete(activityRecords)
      .where(eq(activityRecords.id, activityRecordId!))
      .returning();
    return { activityRecord: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const createActivityRecordWithRelations = async (
  activityRecord: NewActivityRecordParams,
  tags: NewTag[],
  otherParties: UserId[],
) => {
  const newActivityRecord = insertActivityRecordSchema.parse(activityRecord);
  // const testActivity = updateActivitySchema.parse({ name: 'new test 2', description: 'this is a new test', activityGroup: "grants", codes: "collections", locationImportant: true, deliveryMethodImportant: true, id: generate_uuid4() })
  // const quantities: QuantityId[] = ["097fad3c-a0ef-4a79-adbc-8bd304f7c786", "63dcb5dd-c70e-48fb-be91-a97e55545b37"];
  // const departments: DepartmentId[] = ["7ff95350-a4ed-4672-bbd2-82c828a205ba"];
  try {
    const [a] = await db.transaction(async (tx) => {
      const res = await tx
        .insert(activityRecords)
        .values(newActivityRecord)
        .returning();
      //?: UPSERT into tag table with tag objects
      const upsertedTags = tags.map((tag) => ({
        tag: tag.tag,
        id: tag.id || generate_uuid4(),
      }));
      if (upsertedTags && upsertedTags.length > 0) {
        await tx
          .insert(tagsTable)
          .values(upsertedTags)
          .onConflictDoNothing()
          .execute();
        await tx
          .insert(tagsToActivityRecords)
          .values(
            upsertedTags.map((tagId) => ({
              activityRecordId: res[0].id,
              tagId: tagId.id,
            })),
          )
          .execute();
      }
      if (otherParties && otherParties.length > 0) {
        await tx
          .insert(otherPartiesToActivityRecords)
          .values(
            otherParties.map((otherPartyId) => ({
              activityRecordId: res[0].id,
              otherPartyId: otherPartyId,
            })),
          )
          .execute();
      }
      return res;
    });
    return { activityRecord: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};
