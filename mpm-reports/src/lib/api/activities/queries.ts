import { db, InferQueryModel } from "@/lib/db/index";
import { eq, inArray } from "drizzle-orm";
import {
  type ActivityId,
  activityIdSchema,
  activities,
  activitiesToDepartments,
  activitiesToQuantities,
} from "@/lib/db/schema/activities";
import { activityRecords } from "@/lib/db/schema/activityRecords";
import { Department, departments } from "@/lib/db/schema/departments";
import { quantities } from "@/lib/db/schema/quantities";

// export const getActivities = async () => {
//   const rows = await db
//     .select({ activity: activities, activityRecord: activityRecords })
//     .from(activities);
//   const a = rows.map((r) => ({
//     ...r.activity,
//     activityRecord: r.activityRecord,
//   }));
//   return { activities: a };
// };

export const getActivities = async () => {
  const rows = await db.select({ activity: activities }).from(activities);
  const a = rows.map((r) => ({
    ...r.activity,
  }));
  return { activities: a };
};

export const getActivityById = async (id: ActivityId) => {
  const { id: activityId } = activityIdSchema.parse({ id });
  const [row] = await db
    .select({ activity: activities })
    .from(activities)
    .where(eq(activities.id, activityId));
  if (row === undefined) return {};
  const a = { ...row.activity };
  return { activity: a };
};

export const getActivityByIdWithActivityRecords = async (id: ActivityId) => {
  const { id: activityId } = activityIdSchema.parse({ id });
  const [row] = await db
    .select({ activity: activities, activityRecord: activityRecords })
    .from(activityRecords)
    .where(eq(activities.id, activityId));
  return { activity: row.activity, activityRecords: row.activityRecord };
};

// ? SEE ISSUE : https://github.com/drizzle-team/drizzle-orm/discussions/1152#discussioncomment-8657609
export const getActivitiesByDepartmentWithRelations = async (
  departments: Department[],
) => {
  if (departments.length === 0) return {};
  const rows = await db.query.activities.findMany({
    with: {
      activitiesToDepartments: {
        with: {
          department: true,
        },
        //! this currently only filters out the activitiesToDepartments contents, not the full record
        //! NOTE: this is still important to filter down the "Department" dropdown options on RecordNewActivityForm
        where: inArray(
          activitiesToDepartments.departmentId,
          departments.map((d) => d.id),
        ),
      },
      activitiesToQuantities: {
        with: {
          quantity: true,
        },
      },
    },
  });
  // .then((res) => res.filter(
  //   ({ activitiesToDepartments }) => activitiesToDepartments.some(dept => departments.includes(dept.department.name)),
  // ));
  if (rows.length === 0) return {};
  return { rows };
};

export const getActivitiesByDepartments = async (
  departmentList: Department[],
) => {
  if (departmentList.length === 0) return {};

  const rows = await db
    .select({ activity: activities })
    .from(activities)
    .leftJoin(
      activitiesToDepartments,
      eq(activitiesToDepartments.activityId, activities.id),
    )
    .leftJoin(
      activitiesToQuantities,
      eq(activitiesToQuantities.activityId, activities.id),
    )
    .innerJoin(quantities, eq(activitiesToQuantities.quantityId, quantities.id))
    .innerJoin(
      departments,
      eq(activitiesToDepartments.departmentId, departments.id),
    )
    .where(
      inArray(
        activitiesToDepartments.departmentId,
        departmentList.map((d) => d.id),
      ),
    );
  if (rows === undefined) return {};
  return { rows };
};

export type ActivitiesWithRelations = InferQueryModel<
  "activities",
  {
    id: true;
    name: true;
    codes: true;
    activityGroup: true;
    locationImportant: true;
    deliveryMethodImportant: true;
    createdAt: true;
    updatedAt: true;
  },
  {
    activitiesToQuantities: {
      with: {
        quantity: true;
      };
    };
    activitiesToDepartments: {
      with: {
        department: true;
      };
    };
  }
>;
