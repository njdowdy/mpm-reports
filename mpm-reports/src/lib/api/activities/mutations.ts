import { db } from "@/lib/db/index";
import { eq } from "drizzle-orm";
import {
  ActivityId,
  NewActivityParams,
  UpdateActivityParams,
  updateActivitySchema,
  insertActivitySchema,
  activities,
  activityIdSchema,
  activitiesToDepartments,
  activitiesToQuantities,
} from "@/lib/db/schema/activities";
import { QuantityId } from "@/lib/db/schema/quantities";
import { DepartmentId } from "@/lib/db/schema/departments";

export const createActivity = async (activity: NewActivityParams) => {
  const newActivity = insertActivitySchema.parse(activity);
  try {
    const [a] = await db.insert(activities).values(newActivity).returning();
    return { activity: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const updateActivity = async (
  id: ActivityId,
  activity: UpdateActivityParams,
) => {
  const { id: activityId } = activityIdSchema.parse({ id });
  const newActivity = updateActivitySchema.parse(activity);
  try {
    const [a] = await db
      .update(activities)
      .set({ ...newActivity, updatedAt: new Date() })
      .where(eq(activities.id, activityId!))
      .returning();
    return { activity: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const deleteActivity = async (id: ActivityId) => {
  const { id: activityId } = activityIdSchema.parse({ id });
  try {
    const [a] = await db
      .delete(activities)
      .where(eq(activities.id, activityId!))
      .returning();
    return { activity: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const createActivityWithRelations = async (
  activity: NewActivityParams,
  quantities: QuantityId[],
  departments: DepartmentId[],
) => {
  const newActivity = insertActivitySchema.parse(activity);
  // const testActivity = updateActivitySchema.parse({ name: 'new test 2', description: 'this is a new test', activityGroup: "grants", codes: "collections", locationImportant: true, deliveryMethodImportant: true, id: generate_uuid4() })
  // const quantities: QuantityId[] = ["097fad3c-a0ef-4a79-adbc-8bd304f7c786", "63dcb5dd-c70e-48fb-be91-a97e55545b37"];
  // const departments: DepartmentId[] = ["7ff95350-a4ed-4672-bbd2-82c828a205ba"];
  try {
    const [a] = await db.transaction(async (tx) => {
      const res = await tx.insert(activities).values(newActivity).returning();
      await tx
        .insert(activitiesToDepartments)
        .values(
          departments.map((departmentId) => ({
            activityId: res[0].id,
            departmentId: departmentId,
          })),
        )
        .execute();
      await tx
        .insert(activitiesToQuantities)
        .values(
          quantities.map((quantityId) => ({
            activityId: res[0].id,
            quantityId: quantityId,
          })),
        )
        .execute();
      return res;
    });
    return { activity: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};
