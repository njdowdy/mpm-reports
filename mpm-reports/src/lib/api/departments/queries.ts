import { db } from "@/lib/db/index";
import { departments, departmentSchema } from "@/lib/db/schema/departments";
import {
  UserId,
  userIdSchema,
  users,
  usersToDepartments,
} from "@/lib/db/schema/users";
import { type DepartmentEnum } from "@/lib/db/schema/departments";
import { eq } from "drizzle-orm";

export const getDepartmentByName = async (name: DepartmentEnum) => {
  const { name: department } = departmentSchema.parse({ name });
  const [row] = await db
    .select({ department: departments })
    .from(departments)
    .where(eq(departments.name, department));
  if (row === undefined) return {};
  const a = { ...row.department };
  return { department: a };
};

export const getDepartments = async () => {
  const rows = await db.select({ department: departments }).from(departments);
  const a = rows.map((r) => ({
    ...r.department,
  }));
  return { departments: a };
};

export const getDepartmentsByUserId = async (id: UserId) => {
  const { id: userId } = userIdSchema.parse({ id });
  const rows = await db
    .select({ department: departments })
    .from(departments)
    .leftJoin(
      usersToDepartments,
      eq(usersToDepartments.departmentId, departments.id),
    )
    .where(eq(usersToDepartments.userId, userId));
  if (rows === undefined) return {};
  // const u = rows;
  const a = rows.map((r) => ({
    ...r.department,
  }));
  return { departments: a };
};
