import { db } from "@/lib/db/index";
import { eq } from "drizzle-orm";
import {
  DepartmentId,
  NewDepartmentParams,
  UpdateDepartmentParams,
  updateDepartmentSchema,
  insertDepartmentSchema,
  departments,
  departmentIdSchema,
} from "@/lib/db/schema/departments";

export const createDepartment = async (department: NewDepartmentParams) => {
  const newDepartment = insertDepartmentSchema.parse(department);
  try {
    const [a] = await db.insert(departments).values(newDepartment).returning();
    return { department: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const updateDepartment = async (
  id: DepartmentId,
  department: UpdateDepartmentParams,
) => {
  const { id: departmentId } = departmentIdSchema.parse({ id });
  const newDepartment = updateDepartmentSchema.parse(department);
  try {
    const [a] = await db
      .update(departments)
      .set({ ...newDepartment, updatedAt: new Date() })
      .where(eq(departments.id, departmentId!))
      .returning();
    return { department: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};

export const deleteDepartment = async (id: DepartmentId) => {
  const { id: departmentId } = departmentIdSchema.parse({ id });
  try {
    const [a] = await db
      .delete(departments)
      .where(eq(departments.id, departmentId!))
      .returning();
    return { department: a };
  } catch (err) {
    const message = (err as Error).message ?? "Error, please try again";
    console.error(message);
    throw { error: message };
  }
};
