import { clsx, type ClassValue } from "clsx";
import { twMerge } from "tailwind-merge";
import { v4 as uuidv4 } from "uuid";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export async function getBaseUrl() {
  return process.env.VERCEL_ENV === "production"
    ? `https://reports.mpm.tools`
    : process.env.VERCEL_URL
      ? `https://${process.env.VERCEL_URL}`
      : `https://localhost:3000`;
}

export const calculateFiscalQuarter = async (datePerformedEnd: Date) => {
  const year = datePerformedEnd.getFullYear();
  const month = datePerformedEnd.getMonth() + 1;
  let quarter: number;
  let fiscalyear: number = year;

  if (month >= 9 && month <= 11) {
    quarter = 1;
    fiscalyear = year + 1;
  } else if (month >= 12 || month <= 2) {
    quarter = 2;
    if (month === 12) {
      fiscalyear = year + 1;
    }
  } else if (month >= 3 && month <= 5) {
    quarter = 3;
  } else {
    quarter = 4;
  }
  return `FY${fiscalyear}Q${quarter}`;
};

export const generate_uuid4 = () => uuidv4();

export const timestamps: { createdAt: true; updatedAt: true } = {
  createdAt: true,
  updatedAt: true,
};

export type Action = "create" | "update" | "delete";

export type OptimisticAction<T> = {
  action: Action;
  data: T;
};

//?: See: https://github.com/colinhacks/zod/discussions/2125#discussioncomment-7452235
export function getEnumValues<T extends Record<string, any>>(obj: T) {
  return Object.values(obj) as [(typeof obj)[keyof T]];
}
