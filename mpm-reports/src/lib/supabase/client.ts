import { createBrowserClient } from "@supabase/ssr";
import { generate_uuid4 } from "../utils";

type Buckets = "avatars";

export function createClient() {
  return createBrowserClient(
    process.env.NEXT_PUBLIC_SUPABASE_URL!,
    process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY!,
  );
}

export async function getUserSession() {
  const supabase = createClient();
  const {
    data: { session },
  } = await supabase.auth.getSession();
  return session;
}

//? This needs to be a client function because it uses the cookie store
export async function resetSignedInUserPassword(data: { newPassword: string }) {
  const supabase = createClient();
  const result = await supabase.auth.updateUser({
    password: data.newPassword,
  });
  return JSON.stringify(result);
}

export async function uploadPublicImage(image: File, bucket: Buckets) {
  const filename = `${generate_uuid4()}.${image.type.split("/")[1]}`;
  const supabase = createClient();
  //? you need to ensure RLS is set up properly on the bucket
  const result = await supabase.storage
    .from(bucket)
    .upload(`public/${filename}`, image, {
      cacheControl: "3600",
      upsert: false,
    });
  return JSON.stringify(result);
}
