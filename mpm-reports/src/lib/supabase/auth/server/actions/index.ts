"use server";

import { createClient } from "@/lib/supabase/server";
import { getUserById } from "@/lib/api/users/queries";
import { User } from "@/lib/db/schema/users";

export async function signInWithEmailAndPassword(data: {
  email: string;
  password: string;
}) {
  const supabase = await createClient();

  const res = await supabase.auth.signInWithPassword(data);

  return JSON.stringify(res); // serialize to pass to a client component from server component
}

export async function getCurrentUser(): Promise<User | null> {
  const supabase = await createClient();
  const { data } = await supabase.auth.getUser();

  // Get the user data only if we have an authenticated user
  const userResult = data?.user ? await getUserById(data.user.id) : null;
  return userResult?.user ?? null;
}

export async function requestPasswordResetWithEmail(data: { email: string }) {
  const supabase = await createClient();
  // const result = await supabase.auth.resetPasswordForEmail(data.email, {
  //   redirectTo: `${await getBaseUrl()}/reset-password/`,
  // });
  const result = await supabase.auth.signInWithOtp({
    email: data.email,
    options: {
      // set this to false if you do not want the user to be automatically signed up
      shouldCreateUser: false,
    },
  });
  return JSON.stringify(result);
}

export async function signInWithOtp(data: { email: string; token: string }) {
  const supabase = await createClient();
  const result = await supabase.auth.verifyOtp({
    email: data.email,
    token: data.token,
    type: "email",
  });
  return result;
}

export async function signUpWithEmail(data: {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}) {
  const supabase = await createClient();
  const result = await supabase.auth.signUp({
    email: data.email.toLowerCase(),
    password: data.password,
    options: {
      data: {
        first_name: data.firstName,
        last_name: data.lastName,
        display_name: `${data.firstName} ${data.lastName}`,
      },
    },
  });
  return JSON.stringify(result);
}

export async function signOut() {
  const supabase = await createClient();
  await supabase.auth.signOut();
}
