import { drizzle } from "drizzle-orm/postgres-js";
import postgres from "postgres";
import { env } from "@/lib/env.mjs";
import {
  otherPartiesToActivityRecords,
  otherPartiesToActivityRecordsRelations,
  users,
  usersRelations,
  usersToDepartments,
  usersToDepartmentsRelations,
} from "./schema/users";
import {
  activityRecords,
  activityRecordsRelations,
} from "./schema/activityRecords";
import {
  activities,
  activitiesRelations,
  activitiesToDepartments,
  activitiesToDepartmentsRelations,
  activitiesToQuantities,
  activitiesToQuantitiesRelations,
} from "./schema/activities";
import { departments, departmentsRelations } from "./schema/departments";
import {
  tags,
  tagsRelations,
  tagsToActivityRecords,
  tagsToActivityRecordsRelations,
} from "./schema/tags";
import { quantities, quantitiesRelations } from "./schema/quantities";
import {
  BuildQueryResult,
  DBQueryConfig,
  ExtractTablesWithRelations,
} from "drizzle-orm";

const schema = {
  users,
  usersRelations,
  usersToDepartments,
  usersToDepartmentsRelations,
  otherPartiesToActivityRecords,
  otherPartiesToActivityRecordsRelations,
  activityRecords,
  activityRecordsRelations,
  activities,
  activitiesRelations,
  activitiesToQuantities,
  activitiesToQuantitiesRelations,
  activitiesToDepartments,
  activitiesToDepartmentsRelations,
  departments,
  departmentsRelations,
  quantities,
  quantitiesRelations,
  tags,
  tagsRelations,
  tagsToActivityRecords,
  tagsToActivityRecordsRelations,
};

//? Helper types for infering the return type of 'with' queries
//? See: https://github.com/drizzle-team/drizzle-orm/issues/695#issuecomment-2046191124

type Schema = typeof schema;
type TablesWithRelations = ExtractTablesWithRelations<Schema>;

export type IncludeRelation<TableName extends keyof TablesWithRelations> =
  DBQueryConfig<
    "one" | "many",
    boolean,
    TablesWithRelations,
    TablesWithRelations[TableName]
  >["with"];

export type IncludeColumns<TableName extends keyof TablesWithRelations> =
  DBQueryConfig<
    "one" | "many",
    boolean,
    TablesWithRelations,
    TablesWithRelations[TableName]
  >["columns"];

export type InferQueryModel<
  TableName extends keyof TablesWithRelations,
  Columns extends IncludeColumns<TableName> | undefined = undefined,
  With extends IncludeRelation<TableName> | undefined = undefined,
> = BuildQueryResult<
  TablesWithRelations,
  TablesWithRelations[TableName],
  {
    columns: Columns;
    with: With;
  }
>;

//? see docs: https://supabase.com/docs/guides/database/connecting-to-postgres
const connectionString = env.NEXT_PUBLIC_POSTGRES_URL;
//? { prepare: false } added to troubleshoot supabase connection issue with "Max Clients Exceeded" error
//? however, supabase reported some issues with their service that day, so maybe this is not necessary
const client = postgres(connectionString, { prepare: false });
export const db = drizzle(client, { schema });
