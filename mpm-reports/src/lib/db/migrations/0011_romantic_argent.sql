ALTER TABLE "tags_to_activity_records" DROP CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk";
--> statement-breakpoint
ALTER TABLE "other_parties_to_activity_records" DROP CONSTRAINT "other_parties_to_activity_records_other_party_id_users_id_fk";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tags_to_activity_records" ADD CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk" FOREIGN KEY ("tag_id") REFERENCES "tags"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "other_parties_to_activity_records" ADD CONSTRAINT "other_parties_to_activity_records_other_party_id_users_id_fk" FOREIGN KEY ("other_party_id") REFERENCES "users"("id") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
