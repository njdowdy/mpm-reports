CREATE TABLE IF NOT EXISTS "activities_to_quantities" (
	"activity_id" uuid NOT NULL,
	"quantity_id" uuid NOT NULL,
	"created_at" timestamp DEFAULT now() NOT NULL,
	"updated_at" timestamp DEFAULT now() NOT NULL,
	CONSTRAINT "activities_to_quantities_activity_id_quantity_id_pk" PRIMARY KEY("activity_id","quantity_id")
);
--> statement-breakpoint
ALTER TABLE "activities" DROP COLUMN IF EXISTS "quantity_unit";--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activities_to_quantities" ADD CONSTRAINT "activities_to_quantities_activity_id_activities_id_fk" FOREIGN KEY ("activity_id") REFERENCES "activities"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activities_to_quantities" ADD CONSTRAINT "activities_to_quantities_quantity_id_activities_id_fk" FOREIGN KEY ("quantity_id") REFERENCES "activities"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
