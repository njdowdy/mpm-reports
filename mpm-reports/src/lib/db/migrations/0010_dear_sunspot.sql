ALTER TABLE "tags_to_activity_records" DROP CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tags_to_activity_records" ADD CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk" FOREIGN KEY ("tag_id") REFERENCES "tags"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
