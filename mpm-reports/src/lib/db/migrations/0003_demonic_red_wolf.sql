CREATE TABLE IF NOT EXISTS "tags" (
	"id" uuid PRIMARY KEY NOT NULL,
	"tag" text NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "tags_to_activity_records" (
	"tag_id" uuid NOT NULL,
	"activity_record_id" uuid NOT NULL,
	CONSTRAINT "tags_to_activity_records_tag_id_activity_record_id_pk" PRIMARY KEY("tag_id","activity_record_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tags_to_activity_records" ADD CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk" FOREIGN KEY ("tag_id") REFERENCES "tags"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tags_to_activity_records" ADD CONSTRAINT "tags_to_activity_records_activity_record_id_activity_records_id_fk" FOREIGN KEY ("activity_record_id") REFERENCES "activity_records"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
