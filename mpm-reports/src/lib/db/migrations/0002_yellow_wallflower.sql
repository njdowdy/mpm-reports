ALTER TABLE "activity_records" RENAME COLUMN "user_id" TO "reporting_user_id";--> statement-breakpoint
ALTER TABLE "activity_records" DROP CONSTRAINT "activity_records_user_id_users_id_fk";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activity_records" ADD CONSTRAINT "activity_records_reporting_user_id_users_id_fk" FOREIGN KEY ("reporting_user_id") REFERENCES "users"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
