DO $$ BEGIN
 CREATE TYPE "activityGroup" AS ENUM('collections management', 'community science', 'conservation', 'copywriting', 'exhibits', 'external collection use', 'external service and partnerships', 'externalized data', 'grants', 'insurance and valuations', 'internal service', 'mentoring and supervision', 'native initiatives', 'permitting', 'presentation and instruction', 'professional development', 'programs and events', 'provenance', 'publications', 'repatriation', 'requests', 'support activities', 'vetting and reviewing');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "codes" AS ENUM('collections', 'organizational culture', 'discovery', 'engagement', 'service');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "activities" ALTER COLUMN "codes" SET DATA TYPE codes;--> statement-breakpoint
ALTER TABLE "activities" ADD COLUMN "activityGroup" "activityGroup" NOT NULL;--> statement-breakpoint
ALTER TABLE "activities" DROP COLUMN IF EXISTS "group";