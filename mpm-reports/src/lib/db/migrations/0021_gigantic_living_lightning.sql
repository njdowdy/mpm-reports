ALTER TABLE "activity_records" ADD COLUMN "unit_id" uuid NOT NULL;--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activity_records" ADD CONSTRAINT "activity_records_unit_id_quantities_id_fk" FOREIGN KEY ("unit_id") REFERENCES "quantities"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
