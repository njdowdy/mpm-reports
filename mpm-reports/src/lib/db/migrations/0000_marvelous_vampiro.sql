DO $$ BEGIN
 CREATE TYPE "delivery_method" AS ENUM('virtual', 'in_person', 'not_tracked');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "location" AS ENUM('in_state', 'out_of_state', 'not_tracked');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "department" AS ENUM('zoology', 'botany', 'cultural', 'geology', 'registration', 'development', 'visitor_services', 'retail', 'finance', 'facilities', 'information_services', 'education', 'marketing', 'graphics', 'pbw', 'planetarium', 'exhibits', 'idea', 'unassigned', 'collections_informatics');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "role" AS ENUM('user', 'admin', 'superuser');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "activities" (
	"id" uuid PRIMARY KEY NOT NULL,
	"name" text NOT NULL,
	"group" text NOT NULL,
	"codes" text NOT NULL,
	"quantity_unit" text NOT NULL,
	"location_important" boolean NOT NULL,
	"delivery_method_important" boolean NOT NULL,
	"created_at" timestamp DEFAULT now() NOT NULL,
	"updated_at" timestamp DEFAULT now() NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "activities_to_departments" (
	"activity_id" uuid NOT NULL,
	"department_id" uuid NOT NULL,
	CONSTRAINT "activities_to_departments_activity_id_department_id_pk" PRIMARY KEY("activity_id","department_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "activity_records" (
	"id" uuid PRIMARY KEY NOT NULL,
	"date_performed_start" timestamp with time zone NOT NULL,
	"date_performed_end" timestamp with time zone NOT NULL,
	"quantity" integer NOT NULL,
	"description" text NOT NULL,
	"quarter" text NOT NULL,
	"location" "location" NOT NULL,
	"delivery_method" "delivery_method" NOT NULL,
	"user_id" uuid NOT NULL,
	"activity_id" uuid NOT NULL,
	"created_at" timestamp DEFAULT now() NOT NULL,
	"updated_at" timestamp DEFAULT now() NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "departments" (
	"id" uuid PRIMARY KEY NOT NULL,
	"department" "department" NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "users" (
	"id" uuid PRIMARY KEY NOT NULL,
	"role" "role" DEFAULT 'user' NOT NULL,
	"first_name" text,
	"last_name" text,
	"avatar_url" text,
	"username" text,
	"created_at" timestamp DEFAULT now() NOT NULL,
	"updated_at" timestamp DEFAULT now() NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "users_to_departments" (
	"user_id" uuid NOT NULL,
	"department_id" uuid NOT NULL,
	CONSTRAINT "users_to_departments_user_id_department_id_pk" PRIMARY KEY("user_id","department_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activities_to_departments" ADD CONSTRAINT "activities_to_departments_activity_id_activities_id_fk" FOREIGN KEY ("activity_id") REFERENCES "activities"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activities_to_departments" ADD CONSTRAINT "activities_to_departments_department_id_departments_id_fk" FOREIGN KEY ("department_id") REFERENCES "departments"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activity_records" ADD CONSTRAINT "activity_records_user_id_users_id_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "users_to_departments" ADD CONSTRAINT "users_to_departments_user_id_users_id_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "users_to_departments" ADD CONSTRAINT "users_to_departments_department_id_departments_id_fk" FOREIGN KEY ("department_id") REFERENCES "departments"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
