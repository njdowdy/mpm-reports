DO $$ BEGIN
 CREATE TYPE "department" AS ENUM('zoology', 'botany', 'cultural', 'geology', 'registration', 'development', 'visitor_services', 'retail', 'finance', 'facilities', 'information_services', 'education', 'marketing', 'graphics', 'pbw', 'planetarium', 'exhibits', 'idea', 'unassigned', 'collections_informatics', 'tribal_relations');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
