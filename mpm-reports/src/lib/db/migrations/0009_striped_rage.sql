ALTER TABLE "tags_to_activity_records" DROP CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk";
--> statement-breakpoint
ALTER TABLE "other_parties_to_activity_records" DROP CONSTRAINT "other_parties_to_activity_records_activity_record_id_activity_records_id_fk";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tags_to_activity_records" ADD CONSTRAINT "tags_to_activity_records_tag_id_tags_id_fk" FOREIGN KEY ("tag_id") REFERENCES "tags"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "other_parties_to_activity_records" ADD CONSTRAINT "other_parties_to_activity_records_activity_record_id_activity_records_id_fk" FOREIGN KEY ("activity_record_id") REFERENCES "activity_records"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
