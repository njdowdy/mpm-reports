CREATE TABLE IF NOT EXISTS "other_parties_to_activity_records" (
	"other_party_id" uuid NOT NULL,
	"activity_record_id" uuid NOT NULL,
	CONSTRAINT "other_parties_to_activity_records_other_party_id_activity_record_id_pk" PRIMARY KEY("other_party_id","activity_record_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "other_parties_to_activity_records" ADD CONSTRAINT "other_parties_to_activity_records_other_party_id_users_id_fk" FOREIGN KEY ("other_party_id") REFERENCES "users"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "other_parties_to_activity_records" ADD CONSTRAINT "other_parties_to_activity_records_activity_record_id_activity_records_id_fk" FOREIGN KEY ("activity_record_id") REFERENCES "activity_records"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
