CREATE TABLE IF NOT EXISTS "quantities" (
	"id" uuid PRIMARY KEY NOT NULL,
	"unit" text NOT NULL,
	"created_at" timestamp DEFAULT now() NOT NULL,
	"updated_at" timestamp DEFAULT now() NOT NULL
);
