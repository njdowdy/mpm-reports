ALTER TABLE "activities_to_quantities" DROP CONSTRAINT "activities_to_quantities_quantity_id_activities_id_fk";
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "activities_to_quantities" ADD CONSTRAINT "activities_to_quantities_quantity_id_quantities_id_fk" FOREIGN KEY ("quantity_id") REFERENCES "quantities"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
