import { generate_uuid4, getEnumValues, timestamps } from "@/lib/utils";
import { relations, sql } from "drizzle-orm";
import { pgEnum, pgTable, timestamp, uuid } from "drizzle-orm/pg-core";
import { usersToDepartments } from "./users";
import { activitiesToDepartments } from "./activities";
import { z } from "zod";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { type getDepartments } from "@/lib/api/departments/queries";

export const DepartmentEnum = {
  zoology: "zoology",
  botany: "botany",
  cultural: "cultural",
  geology: "geology",
  registration: "registration",
  development: "development",
  vistor_services: "visitor_services",
  retail: "retail",
  finance: "finance",
  facilities: "facilities",
  information_services: "information_services",
  education: "education",
  marketing: "marketing",
  graphics: "graphics",
  pbw: "pbw",
  planetarium: "planetarium",
  exhibits: "exhibits",
  idea: "idea",
  unassigned: "unassigned",
  collections_informatics: "collections_informatics",
  tribal_relations: "tribal_relations",
} as const;

export type DepartmentEnum =
  (typeof DepartmentEnum)[keyof typeof DepartmentEnum];

export const department_enum = z.enum(getEnumValues(DepartmentEnum));

export const departmentEnum = pgEnum("department", department_enum.options);

export const departments = pgTable("departments", {
  id: uuid("id")
    .primaryKey()
    .$defaultFn(() => generate_uuid4()),
  name: departmentEnum("department").notNull(),
  createdAt: timestamp("created_at")
    .notNull()
    .default(sql`now()`),
  updatedAt: timestamp("updated_at")
    .notNull()
    .default(sql`now()`),
});

export const departmentsRelations = relations(departments, ({ many }) => ({
  usersToDepartments: many(usersToDepartments),
  activitiesToDepartments: many(activitiesToDepartments),
}));

// Schema for departments - used to validate API requests
const baseSchema = createSelectSchema(departments).omit(timestamps);

export const insertDepartmentSchema =
  createInsertSchema(departments).omit(timestamps);
export const insertDepartmentParams = baseSchema.omit({
  id: true,
});

export const updateDepartmentSchema = baseSchema;
export const updateDepartmentParams = baseSchema;
export const departmentIdSchema = baseSchema.pick({ id: true });
export const departmentSchema = baseSchema.pick({ name: true });

// Types for departments - used to type API request params and within Components
export type Department = typeof departments.$inferSelect;
export type NewDepartment = z.infer<typeof insertDepartmentSchema>;
export type NewDepartmentParams = z.infer<typeof insertDepartmentParams>;
export type UpdateDepartmentParams = z.infer<typeof updateDepartmentParams>;
export type DepartmentId = z.infer<typeof departmentIdSchema>["id"];

// this type infers the return from getDepartments() - meaning it will include any joins
export type CompleteDepartment = Awaited<
  ReturnType<typeof getDepartments>
>["departments"][number];
