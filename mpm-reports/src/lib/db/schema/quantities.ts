import { generate_uuid4, timestamps } from "@/lib/utils";
import { relations, sql } from "drizzle-orm";
import { pgTable, text, timestamp, uuid } from "drizzle-orm/pg-core";
import { activitiesToQuantities } from "./activities";
import { z } from "zod";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { type getQuantities } from "@/lib/api/quantities/queries";

export const quantities = pgTable("quantities", {
  id: uuid("id")
    .primaryKey()
    .$defaultFn(() => generate_uuid4()),
  unit: text("unit").notNull(),
  createdAt: timestamp("created_at")
    .notNull()
    .default(sql`now()`),
  updatedAt: timestamp("updated_at")
    .notNull()
    .default(sql`now()`),
});

export const quantitiesRelations = relations(quantities, ({ many }) => ({
  activitiesToQuantities: many(activitiesToQuantities),
}));

// Schema for quantities - used to validate API requests
const baseSchema = createSelectSchema(quantities).omit(timestamps);

export const insertQuantitySchema =
  createInsertSchema(quantities).omit(timestamps);
export const insertQuantityParams = baseSchema.omit({
  id: true,
});

export const updateQuantitySchema = baseSchema;
export const updateQuantityParams = baseSchema;
export const quantityIdSchema = baseSchema.pick({ id: true });
export const quantitySchema = baseSchema.pick({ unit: true });

// Types for quantities - used to type API request params and within Components
export type Quantity = typeof quantities.$inferSelect;
export type NewQuantity = z.infer<typeof insertQuantitySchema>;
export type NewQuantityParams = z.infer<typeof insertQuantityParams>;
export type UpdateQuantityParams = z.infer<typeof updateQuantityParams>;
export type QuantityId = z.infer<typeof quantityIdSchema>["id"];

// this type infers the return from getQuantities() - meaning it will include any joins
export type CompleteQuantity = Awaited<
  ReturnType<typeof getQuantities>
>["quantities"][number];
