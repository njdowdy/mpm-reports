import { generate_uuid4, timestamps } from "@/lib/utils";
import { relations, sql } from "drizzle-orm";
import {
  pgTable,
  primaryKey,
  text,
  timestamp,
  uuid,
} from "drizzle-orm/pg-core";
import { activityRecords } from "./activityRecords";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { z } from "zod";
import { type getTags } from "@/lib/api/tags/queries";

export const tags = pgTable("tags", {
  id: uuid("id")
    .primaryKey()
    .$defaultFn(() => generate_uuid4()),
  tag: text("tag").notNull(),
  createdAt: timestamp("created_at")
    .notNull()
    .default(sql`now()`),
  updatedAt: timestamp("updated_at")
    .notNull()
    .default(sql`now()`),
});

export const tagsRelations = relations(tags, ({ many }) => ({
  tagsToActivityRecords: many(tagsToActivityRecords),
}));

export const tagsToActivityRecords = pgTable(
  "tags_to_activity_records",
  {
    tagId: uuid("tag_id")
      .notNull()
      //? delete connection when tagId is deleted; this did not seem to migrate properly to Supabase, so I changed it manually
      .references(() => tags.id, { onDelete: "cascade", onUpdate: "cascade" }),
    activityRecordId: uuid("activity_record_id")
      .notNull()
      //? delete connection when activityRecord is deleted; this did not seem to migrate properly to Supabase, so I changed it manually
      .references(() => activityRecords.id, {
        onDelete: "cascade",
        onUpdate: "cascade",
      }),
    createdAt: timestamp("created_at")
      .notNull()
      .default(sql`now()`),
    updatedAt: timestamp("updated_at")
      .notNull()
      .default(sql`now()`),
  },
  (t) => ({
    pk: primaryKey({ columns: [t.tagId, t.activityRecordId] }),
  }),
);

export const tagsToActivityRecordsRelations = relations(
  tagsToActivityRecords,
  ({ one }) => ({
    activityRecord: one(activityRecords, {
      fields: [tagsToActivityRecords.activityRecordId],
      references: [activityRecords.id],
    }),
    tag: one(tags, {
      fields: [tagsToActivityRecords.tagId],
      references: [tags.id],
    }),
  }),
);

// Schema for tags - used to validate API requests
const baseSchema = createSelectSchema(tags).omit(timestamps);

export const insertTagSchema = createInsertSchema(tags).omit(timestamps);

export const insertTagParams = baseSchema.omit({
  id: true,
});

export const updateTagSchema = baseSchema;
export const updateTagParams = baseSchema;
export const tagIdSchema = baseSchema.pick({ id: true });

// Types for tags - used to type API request params and within Components
export type Tag = typeof tags.$inferSelect;
export type NewTag = z.infer<typeof insertTagSchema>;
export type NewTagParams = z.infer<typeof insertTagParams>;
export type UpdateTagParams = z.infer<typeof updateTagParams>;
export type TagId = z.infer<typeof tagIdSchema>["id"];

// this type infers the return from getTags() - meaning it will include any joins
export type CompleteTag = Awaited<ReturnType<typeof getTags>>["tags"][number];
