import { relations, sql } from "drizzle-orm";
import {
  boolean,
  timestamp,
  pgTable,
  uuid,
  text,
  primaryKey,
  pgEnum,
} from "drizzle-orm/pg-core";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { z } from "zod";
import { activityRecords } from "./activityRecords";
import { type getActivities } from "@/lib/api/activities/queries";

import { generate_uuid4, getEnumValues, timestamps } from "@/lib/utils";
import { departments } from "./departments";
import { quantities } from "./quantities";

export const CodesEnum = {
  collections: "collections",
  organizational_culture: "organizational culture",
  discovery: "discovery",
  engagement: "engagement",
  service: "service",
} as const;

export type CodesEnum = (typeof CodesEnum)[keyof typeof CodesEnum];

export const codes_enum = z.enum(getEnumValues(CodesEnum));

export const codesEnum = pgEnum("codes", codes_enum.options);

export const ActivityGroupEnum = {
  collections_management: "collections management",
  community_science: "community science",
  conservation: "conservation",
  copywriting: "copywriting",
  exhibits: "exhibits",
  external_collection_use: "external collection use",
  external_service_and_partnerships: "external service and partnerships",
  externalized_data: "externalized data",
  grants: "grants",
  insurance_and_valuations: "insurance and valuations",
  internal_service: "internal service",
  mentoring_and_supervision: "mentoring and supervision",
  native_initiatives: "native initiatives",
  permitting: "permitting",
  presentation_and_instruction: "presentation and instruction",
  professional_development: "professional development",
  programs_and_events: "programs and events",
  provenance: "provenance",
  publications: "publications",
  repatriation: "repatriation",
  requests: "requests",
  support_activities: "support activities",
  vetting_and_reviewing: "vetting and reviewing",
} as const;

export type ActivityGroupEnum =
  (typeof ActivityGroupEnum)[keyof typeof ActivityGroupEnum];

export const activity_group_enum = z.enum(getEnumValues(ActivityGroupEnum));

export const activityGroupEnum = pgEnum(
  "activityGroup",
  activity_group_enum.options,
);

export const activities = pgTable("activities", {
  id: uuid("id")
    .primaryKey()
    .$defaultFn(() => generate_uuid4()),
  name: text("name").notNull(),
  codes: codesEnum("codes").notNull(),
  activityGroup: activityGroupEnum("activityGroup").notNull(),
  locationImportant: boolean("location_important").notNull(),
  deliveryMethodImportant: boolean("delivery_method_important").notNull(),
  createdAt: timestamp("created_at")
    .notNull()
    .default(sql`now()`),
  updatedAt: timestamp("updated_at")
    .notNull()
    .default(sql`now()`),
});

export const activitiesRelations = relations(activities, ({ many }) => ({
  activitiesToDepartments: many(activitiesToDepartments),
  activitiesToActivityRecords: many(activityRecords),
  activitiesToQuantities: many(activitiesToQuantities),
}));

export const activitiesToQuantities = pgTable(
  "activities_to_quantities",
  {
    activityId: uuid("activity_id")
      .notNull()
      .references(() => activities.id),
    quantityId: uuid("quantity_id")
      .notNull()
      .references(() => quantities.id),
    createdAt: timestamp("created_at")
      .notNull()
      .default(sql`now()`),
    updatedAt: timestamp("updated_at")
      .notNull()
      .default(sql`now()`),
  },
  (t) => ({
    pk: primaryKey({ columns: [t.activityId, t.quantityId] }),
  }),
);

export const activitiesToQuantitiesRelations = relations(
  activitiesToQuantities,
  ({ one }) => ({
    quantity: one(quantities, {
      fields: [activitiesToQuantities.quantityId],
      references: [quantities.id],
    }),
    activity: one(activities, {
      fields: [activitiesToQuantities.activityId],
      references: [activities.id],
    }),
  }),
);

export const activitiesToDepartments = pgTable(
  "activities_to_departments",
  {
    activityId: uuid("activity_id")
      .notNull()
      .references(() => activities.id),
    departmentId: uuid("department_id")
      .notNull()
      .references(() => departments.id),
    createdAt: timestamp("created_at")
      .notNull()
      .default(sql`now()`),
    updatedAt: timestamp("updated_at")
      .notNull()
      .default(sql`now()`),
  },
  (t) => ({
    pk: primaryKey({ columns: [t.activityId, t.departmentId] }),
  }),
);

export const activitiesToDepartmentsRelations = relations(
  activitiesToDepartments,
  ({ one }) => ({
    department: one(departments, {
      fields: [activitiesToDepartments.departmentId],
      references: [departments.id],
    }),
    activity: one(activities, {
      fields: [activitiesToDepartments.activityId],
      references: [activities.id],
    }),
  }),
);

// Schema for activities - used to validate API requests
const baseSchema = createSelectSchema(activities).omit(timestamps);

export const insertActivitySchema =
  createInsertSchema(activities).omit(timestamps);
export const insertActivityParams = baseSchema
  .extend({
    locationImportant: z.coerce.boolean(),
    deliveryMethodImportant: z.coerce.boolean(),
    activityRecordId: z.coerce.string().min(1),
  })
  .omit({
    id: true,
  });

export const updateActivitySchema = baseSchema;
export const updateActivityParams = baseSchema.extend({
  locationImportant: z.coerce.boolean(),
  deliveryMethodImportant: z.coerce.boolean(),
  activityRecordId: z.coerce.string().min(1),
});
export const activityIdSchema = baseSchema.pick({ id: true });

// Types for activities - used to type API request params and within Components
export type Activity = typeof activities.$inferSelect;
export type NewActivity = z.infer<typeof insertActivitySchema>;
export type NewActivityParams = z.infer<typeof insertActivityParams>;
export type UpdateActivityParams = z.infer<typeof updateActivityParams>;
export type ActivityId = z.infer<typeof activityIdSchema>["id"];

// this type infers the return from getActivities() - meaning it will include any joins
export type CompleteActivity = Awaited<
  ReturnType<typeof getActivities>
>["activities"][number];
