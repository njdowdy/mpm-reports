import { relations, sql } from "drizzle-orm";
import {
  varchar,
  timestamp,
  pgTable,
  uuid,
  pgEnum,
  primaryKey,
  text,
} from "drizzle-orm/pg-core";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { z } from "zod";

import { type getUsers } from "@/lib/api/users/queries";

import { generate_uuid4, timestamps } from "@/lib/utils";
import { departments } from "./departments";
import { activityRecords } from "./activityRecords";

export const roleEnum = pgEnum("role", ["user", "admin", "superuser"]);

export const users = pgTable("users", {
  id: uuid("id")
    .primaryKey()
    .$defaultFn(() => generate_uuid4()),
  role: roleEnum("role").default("user").notNull(),
  firstName: text("first_name"),
  lastName: text("last_name"),
  avatarUrl: text("avatar_url"),
  username: text("username").notNull().unique(),
  email: text("email").notNull().unique(),
  createdAt: timestamp("created_at")
    .notNull()
    .default(sql`now()`),
  updatedAt: timestamp("updated_at")
    .notNull()
    .default(sql`now()`),
});

export const usersRelations = relations(users, ({ many }) => ({
  usersToDepartments: many(usersToDepartments),
  otherPartiesToActivityRecords: many(otherPartiesToActivityRecords),
}));

export const otherPartiesToActivityRecords = pgTable(
  "other_parties_to_activity_records",
  {
    otherPartyId: uuid("other_party_id")
      .notNull()
      //? delete connection when otherPartyId is deleted; this did not seem to migrate properly to Supabase, so I changed it manually
      .references(() => users.id, { onDelete: "cascade", onUpdate: "cascade" }),
    activityRecordId: uuid("activity_record_id")
      .notNull()
      //? delete connection when activityRecord is deleted; this did not seem to migrate properly to Supabase, so I changed it manually
      .references(() => activityRecords.id, {
        onDelete: "cascade",
        onUpdate: "cascade",
      }),
    createdAt: timestamp("created_at")
      .notNull()
      .default(sql`now()`),
    updatedAt: timestamp("updated_at")
      .notNull()
      .default(sql`now()`),
  },
  (t) => ({
    pk: primaryKey({ columns: [t.otherPartyId, t.activityRecordId] }),
  }),
);

export const otherPartiesToActivityRecordsRelations = relations(
  otherPartiesToActivityRecords,
  ({ one }) => ({
    activityRecord: one(activityRecords, {
      fields: [otherPartiesToActivityRecords.activityRecordId],
      references: [activityRecords.id],
    }),
    otherParty: one(users, {
      fields: [otherPartiesToActivityRecords.otherPartyId],
      references: [users.id],
    }),
  }),
);

export const usersToDepartments = pgTable(
  "users_to_departments",
  {
    userId: uuid("user_id")
      .notNull()
      .references(() => users.id),
    departmentId: uuid("department_id")
      .notNull()
      .references(() => departments.id),
    createdAt: timestamp("created_at")
      .notNull()
      .default(sql`now()`),
    updatedAt: timestamp("updated_at")
      .notNull()
      .default(sql`now()`),
  },
  (t) => ({
    pk: primaryKey({ columns: [t.userId, t.departmentId] }),
  }),
);

export const usersToDepartmentsRelations = relations(
  usersToDepartments,
  ({ one }) => ({
    department: one(departments, {
      fields: [usersToDepartments.departmentId],
      references: [departments.id],
    }),
    user: one(users, {
      fields: [usersToDepartments.userId],
      references: [users.id],
    }),
  }),
);

// Schema for users - used to validate API requests
const baseSchema = createSelectSchema(users).omit(timestamps);

export const insertUserSchema = createInsertSchema(users).omit(timestamps);
export const insertUserParams = baseSchema.extend({}).omit({
  id: true,
});

export const updateUserSchema = baseSchema;
export const updateUserParams = baseSchema.extend({});
export const userIdSchema = baseSchema.pick({ id: true });
export const userUsernameSchema = baseSchema.pick({ username: true });

// Types for users - used to type API request params and within Components
export type User = typeof users.$inferSelect;
export type NewUser = z.infer<typeof insertUserSchema>;
export type NewUserParams = z.infer<typeof insertUserParams>;
export type UpdateUserParams = z.infer<typeof updateUserParams>;
export type UserId = z.infer<typeof userIdSchema>["id"];

// this type infers the return from getUsers() - meaning it will include any joins
export type CompleteUser = Awaited<
  ReturnType<typeof getUsers>
>["users"][number];
