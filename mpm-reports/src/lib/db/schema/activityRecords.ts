import { relations, sql } from "drizzle-orm";
import {
  timestamp,
  integer,
  text,
  pgTable,
  uuid,
  pgEnum,
} from "drizzle-orm/pg-core";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { z } from "zod";
import { otherPartiesToActivityRecords, users } from "./users";
import { getActivityRecords } from "@/lib/api/activityRecords/queries";

import { generate_uuid4, timestamps } from "@/lib/utils";

import { activities } from "./activities";
import { tagsToActivityRecords } from "./tags";
import { departments } from "./departments";
import { quantities } from "./quantities";

export type LocationEnum = "in_state" | "out_of_state" | "not_tracked";

export const locationEnum = pgEnum("location", [
  "in_state",
  "out_of_state",
  "not_tracked",
]);

export const deliveryMethodEnum = pgEnum("delivery_method", [
  "virtual",
  "in_person",
  "not_tracked",
]);

//? Changes to this may need to reflected in the data-table component
export const activityRecords = pgTable("activity_records", {
  id: uuid("id")
    .primaryKey()
    .$defaultFn(() => generate_uuid4()),
  reportingUserId: uuid("reporting_user_id")
    .notNull()
    .references(() => users.id),
  datePerformedStart: timestamp("date_performed_start", {
    withTimezone: true,
  }).notNull(),
  datePerformedEnd: timestamp("date_performed_end", {
    withTimezone: true,
  }).notNull(),
  quantity: integer("quantity").notNull(),
  description: text("description").notNull(),
  quarter: text("quarter").notNull(),
  unitId: uuid("unit_id")
    .notNull()
    .references(() => quantities.id),
  departmentId: uuid("department_id")
    .notNull()
    .references(() => departments.id),
  location: locationEnum("location").notNull(),
  deliveryMethod: deliveryMethodEnum("delivery_method").notNull(),
  activityId: uuid("activity_id").notNull(),
  createdAt: timestamp("created_at")
    .notNull()
    .default(sql`now()`),
  updatedAt: timestamp("updated_at")
    .notNull()
    .default(sql`now()`),
});

export const activityRecordsRelations = relations(
  activityRecords,
  ({ one, many }) => ({
    // TODO: not sure if you need to add .references(() => activities.id) to activityId above
    // TODO: or if you can remove the .references from reportingUserId above (i.e., redudant with the one() below)
    activity: one(activities, {
      fields: [activityRecords.activityId],
      references: [activities.id],
    }),
    reportingUser: one(users, {
      fields: [activityRecords.reportingUserId],
      references: [users.id],
    }),
    department: one(departments, {
      fields: [activityRecords.departmentId],
      references: [departments.id],
    }),
    unit: one(quantities, {
      fields: [activityRecords.unitId],
      references: [quantities.id],
    }),
    otherPartiesToActivityRecords: many(otherPartiesToActivityRecords),
    tagsToActivityRecords: many(tagsToActivityRecords),
  }),
);

// Schema for activityRecords - used to validate API requests
const baseSchema = createSelectSchema(activityRecords).omit(timestamps);

export const insertActivityRecordSchema =
  createInsertSchema(activityRecords).omit(timestamps);
export const insertActivityRecordParams = baseSchema
  .extend({
    datePerformedStart: z.coerce
      .date()
      .min(new Date(2015, 0, 1), {
        message: "Starting date must be after 2015",
      })
      .max(new Date(), { message: "Starting date cannot be in the future" }),
    datePerformedEnd: z.coerce
      .date()
      .min(new Date(2015, 0, 1), { message: "Ending date must be after 2015" })
      .max(new Date(), { message: "Ending date cannot be in the future" }),
    quantity: z.coerce
      .number()
      .min(1, { message: "Quantity must be at least 1" }),
  })
  .omit({
    id: true,
  });

export const updateActivityRecordSchema = baseSchema;
export const updateActivityRecordParams = baseSchema.extend({
  datePerformedStart: z.coerce
    .date()
    .min(new Date(2015, 0, 1))
    .max(new Date()),
  datePerformedEnd: z.coerce
    .date()
    .min(new Date(2015, 0, 1))
    .max(new Date()),
  quantity: z.coerce.number(),
  userId: z.coerce.string().min(1).uuid(),
});
export const activityRecordIdSchema = baseSchema.pick({ id: true });

// Types for activityRecords - used to type API request params and within Components
export type ActivityRecord = typeof activityRecords.$inferSelect;
export type NewActivityRecord = z.infer<typeof insertActivityRecordSchema>;
export type NewActivityRecordParams = z.infer<
  typeof insertActivityRecordParams
>;
export type UpdateActivityRecordParams = z.infer<
  typeof updateActivityRecordParams
>;
export type ActivityRecordId = z.infer<typeof activityRecordIdSchema>["id"];

// this type infers the return from getActivityRecords() - meaning it will include any joins
export type CompleteActivityRecord = Awaited<
  ReturnType<typeof getActivityRecords>
>["activityRecords"][number];
