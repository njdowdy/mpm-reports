import type { Metadata } from "next";
import { GeistSans } from "geist/font/sans";
import { GeistMono } from "geist/font/mono";
import "./globals.css";
import { ThemeProvider } from "@/components/ThemeProvider";
// import { getUserSession } from "@/lib/supabase/auth/server/actions";
import { Analytics } from "@/lib/vercel/analytics";
import { Toaster } from "@/components/ui/sonner";
import Navbar from "@/components/shared/Navbar";
import Footer from "@/components/shared/Footer";
export const metadata: Metadata = {
  title: "MPM CREEPI Reports",
  description: "Activity Reporting for MPM CREEPI Departments",
};

export default async function RootLayout({
  children,
  modal,
}: Readonly<{
  children: React.ReactNode;
  modal: React.ReactNode;
}>) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body
        className={`${GeistSans.variable} ${GeistMono.variable} flex flex-col min-h-screen`}
      >
        <ThemeProvider
          attribute="class"
          defaultTheme="system"
          enableSystem
          disableTransitionOnChange
        >
          <div className="flex flex-col flex-grow">
            <Navbar />
            <main className="container min-w-[320px] md:w-full">
              {children}
            </main>
            {modal}
          </div>
          <Footer />
          <Toaster richColors closeButton={true} />
          <Analytics />
        </ThemeProvider>
      </body>
    </html>
  );
}
