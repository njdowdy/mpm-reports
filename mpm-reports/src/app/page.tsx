import Image from "next/image";
import Link from "next/link";
import heroImage from "../../public/images/hero.jpg";

export default async function Home() {
  return (
    <div className="flex flex-col min-h-screen">
      <section className="w-full py-6 sm:py-12 md:py-24 lg:py-32 xl:py-48">
        <div className="container px-4 md:px-6">
          <div className="grid gap-6 lg:grid-cols-[1fr_400px] lg:gap-12 xl:grid-cols-[1fr_600px]">
            <div className="mx-auto overflow-hidden rounded-xl shadow-2xl object-cover sm:w-full lg:order-last transition ease-in-out delay-50 hover:scale-105">
              <Image alt="Tyrannosaurus rex skull" priority src={heroImage} />
            </div>
            <div className="flex flex-col justify-center space-y-4">
              <div className="space-y-2">
                <h1 className="text-3xl font-bold tracking-tighter sm:text-5xl xl:text-6xl/none">
                  CREEPI Reports
                </h1>
                <h2 className="text-xl tracking-tighter sm:text-2xl xl:text-3xl/none">
                  Making quarterly reports extinct
                </h2>
                <p className="max-w-[500px] text-neutral-500 md:text-xl dark:text-neutral-400">
                  Simplify your reporting process. Record activities and
                  generate summaries with ease.
                </p>
              </div>
              <div className="flex flex-col gap-2 min-[400px]:flex-row"></div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
