import { NextResponse } from "next/server";
import { revalidatePath } from "next/cache";
import { z } from "zod";

import {
  createActivity,
  deleteActivity,
  updateActivity,
} from "@/lib/api/activities/mutations";
import {
  activityIdSchema,
  insertActivityParams,
  updateActivityParams,
} from "@/lib/db/schema/activities";

export async function POST(req: Request) {
  try {
    const validatedData = insertActivityParams.parse(await req.json());
    const { activity } = await createActivity(validatedData);

    revalidatePath("/activities"); // optional - assumes you will have named route same as entity

    return NextResponse.json(activity, { status: 201 });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return NextResponse.json({ error: err.issues }, { status: 400 });
    } else {
      return NextResponse.json({ error: err }, { status: 500 });
    }
  }
}

export async function PUT(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const id = searchParams.get("id");

    const validatedData = updateActivityParams.parse(await req.json());
    const validatedParams = activityIdSchema.parse({ id });

    const { activity } = await updateActivity(
      validatedParams.id,
      validatedData,
    );

    return NextResponse.json(activity, { status: 200 });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return NextResponse.json({ error: err.issues }, { status: 400 });
    } else {
      return NextResponse.json(err, { status: 500 });
    }
  }
}

export async function DELETE(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const id = searchParams.get("id");

    const validatedParams = activityIdSchema.parse({ id });
    const { activity } = await deleteActivity(validatedParams.id);

    return NextResponse.json(activity, { status: 200 });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return NextResponse.json({ error: err.issues }, { status: 400 });
    } else {
      return NextResponse.json(err, { status: 500 });
    }
  }
}
