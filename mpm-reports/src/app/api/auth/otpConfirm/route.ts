import { type EmailOtpType } from "@supabase/supabase-js";
import { NextRequest, NextResponse } from "next/server";
import { createClient } from "@/lib/supabase/server";
import { z } from "zod";
// import { toast } from "sonner";

export const dynamic = "force-dynamic";

export async function GET(request: NextRequest) {
  console.log("OTP confirmation route accessed");
  try {
    const { searchParams } = new URL(request.url);
    const tokenHash = searchParams.get("token_hash");
    const token = searchParams.get("token");
    const email = z.string().email().parse(searchParams.get("email"));
    const type = searchParams.get("type") as EmailOtpType | null;
    const next = searchParams.get("next") ?? "/";
    const redirectTo = request.nextUrl.clone();
    redirectTo.pathname = next;

    console.log("Search params:", { tokenHash, type, next });
    if (tokenHash && type && token && email) {
      const supabase = await createClient();
      const { data, error } = await supabase.auth.verifyOtp({
        token: token,
        type: type,
        email: email,
      });

      if (!error) {
        return NextResponse.redirect(redirectTo);
      }

      if (error) {
        return NextResponse.json(
          { error: "OTP verification failed", details: error },
          { status: 400 },
        );
      }
    } else {
      return NextResponse.json(
        { error: "Missing token_hash or type" },
        { status: 400 },
      );
    }
  } catch (error) {
    return NextResponse.json(
      { error: "Internal server error" },
      { status: 500 },
    );
  }
}
