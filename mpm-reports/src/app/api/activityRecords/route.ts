import { NextResponse } from "next/server";
import { revalidatePath } from "next/cache";
import { z } from "zod";

import {
  createActivityRecord,
  deleteActivityRecord,
  updateActivityRecord,
} from "@/lib/api/activityRecords/mutations";
import {
  activityRecordIdSchema,
  insertActivityRecordParams,
  updateActivityRecordParams,
} from "@/lib/db/schema/activityRecords";

export async function POST(req: Request) {
  try {
    const validatedData = insertActivityRecordParams.parse(await req.json());
    const { activityRecord } = await createActivityRecord(validatedData);

    revalidatePath("/activityRecords"); // optional - assumes you will have named route same as entity

    return NextResponse.json(activityRecord, { status: 201 });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return NextResponse.json({ error: err.issues }, { status: 400 });
    } else {
      return NextResponse.json({ error: err }, { status: 500 });
    }
  }
}

export async function PUT(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const id = searchParams.get("id");

    const validatedData = updateActivityRecordParams.parse(await req.json());
    const validatedParams = activityRecordIdSchema.parse({ id });

    const { activityRecord } = await updateActivityRecord(
      validatedParams.id,
      validatedData,
    );

    return NextResponse.json(activityRecord, { status: 200 });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return NextResponse.json({ error: err.issues }, { status: 400 });
    } else {
      return NextResponse.json(err, { status: 500 });
    }
  }
}

export async function DELETE(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const id = searchParams.get("id");

    const validatedParams = activityRecordIdSchema.parse({ id });
    const { activityRecord } = await deleteActivityRecord(validatedParams.id);

    return NextResponse.json(activityRecord, { status: 200 });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return NextResponse.json({ error: err.issues }, { status: 400 });
    } else {
      return NextResponse.json(err, { status: 500 });
    }
  }
}
