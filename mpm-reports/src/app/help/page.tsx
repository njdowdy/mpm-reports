import "server-only";

export default function Help() {
  return (
    <div className="mx-auto flex flex-col items-center mt-10">
      <div className="flex flex-col items-right">
        <h1 className="text-5xl font-bold mb-8">We&apos;re here to help!</h1>
        <div className="max-w-prose flex flex-col items-right gap-8">
          <div className="flex flex-col gap-3">
            <h3 className="text-2xl font-semibold rounded-lg">
              How do I get started?
            </h3>
            <div className="flex flex-col gap-2">
              <p className="text-balance">
                Click &quot;Sign in&quot; in the upper right corner and either
                sign in to an existing account, or click &quot;Sign up&quot; to
                create an account if you don&apos;t have one yet.
              </p>
              <p className="text-balance">
                You will recieve an email requesting you to confirm your
                account. You must complete this before you can sign in for the
                first time.
              </p>
            </div>
          </div>
          <div className="flex flex-col gap-3">
            <h3 className="text-2xl font-semibold rounded-lg">
              How do I record new activity?
            </h3>
            <div className="flex flex-col gap-1">
              <p className="text-balance">
                Once signed in, click your user button in the top right corner.
                From the dropdown menu, select{" "}
                <span className="font-semibold">
                  &quot;Report New Activity&quot;
                </span>
                .
              </p>
            </div>
          </div>
          <div className="flex flex-col gap-3">
            <h3 className="text-2xl font-semibold rounded-lg">
              How do I view activities I have already submitted?
            </h3>
            <div className="flex flex-col gap-1">
              <p className="text-balance">
                Once signed in, click your user button in the top right corner.
                From the dropdown menu, select{" "}
                <span className="font-semibold">
                  &quot;View Reported Activities&quot;
                </span>
                .
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
