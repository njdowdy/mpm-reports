import "server-only";
import ResetPasswordForm from "@/components/auth/ResetPasswordForm";

export default async function ResetPassword() {
  return (
    <div className="mx-auto w-[700px]">
      <h1 className="text-2xl mb-4">Reset Your Password</h1>
      <ResetPasswordForm />
    </div>
  );
}
