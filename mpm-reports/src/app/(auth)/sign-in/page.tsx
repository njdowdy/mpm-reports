"use client";

import SignInForm from "@/components/auth/SignInForm";
import Link from "next/link";

export default function SignIn() {
  return (
    <div className="mx-auto flex w-[300px] flex-col gap-5 pt-10 md:w-[500px]">
      <h1 className="text-4xl">Welcome back!</h1>
      <span className="text-xl">
        Sign in to an existing account with your email and password.
      </span>
      <span>
        Need an account?{" "}
        <Link href="/sign-up" className="hover:underline font-semibold">
          Sign up
        </Link>
      </span>
      <SignInForm />
    </div>
  );
}
