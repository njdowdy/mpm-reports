"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { useRouter, useSearchParams } from "next/navigation";
import { toast } from "sonner";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { signInWithOtp } from "@/lib/supabase/auth/server/actions";

const formSchema = z.object({
  otp: z
    .string()
    .length(6, "OTP must be 6 digits")
    .regex(/^\d+$/, "OTP must contain only numbers"),
});

type VerifyOtpFormType = z.infer<typeof formSchema>;

export default function VerifyOtpPage() {
  const router = useRouter();
  const searchParams = useSearchParams();

  const form = useForm<VerifyOtpFormType>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      otp: "",
    },
  });

  async function onSubmit(formData: VerifyOtpFormType) {
    try {
      const emailParam = searchParams.get("email");
      const email = z.string().email().parse(emailParam);

      const res = await signInWithOtp({ email, token: formData.otp });

      if (res.error) {
        toast.error("Invalid OTP. Please try again.");
        return;
      }

      toast.success("OTP verified successfully");
      router.push("/reset-password");
    } catch (error) {
      console.error("Error verifying OTP:", error);
      toast.error("An error occurred. Please try again.");
    }
  }

  return (
    <div className="max-w-md mx-auto mt-8">
      <h1 className="text-2xl font-bold mb-4">Enter your One-Time Passcode</h1>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
          <FormField
            control={form.control}
            name="otp"
            render={({ field }) => (
              <FormItem>
                <FormLabel>One-Time Passcode</FormLabel>
                <FormControl>
                  <Input
                    type="text"
                    placeholder="Enter 6-digit OTP"
                    autoComplete="one-time-code"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <Button type="submit" size="sm">
            Verify OTP
          </Button>
        </form>
      </Form>
    </div>
  );
}
