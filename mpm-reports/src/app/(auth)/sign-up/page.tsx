"use client";

import SignUpForm from "@/components/auth/SignUpForm";
import Link from "next/link";

export default function SignUp() {
  return (
    <div className="mx-auto flex w-[300px] flex-col gap-2 pt-10 md:w-[500px]">
      <h1 className="text-4xl">Join us!</h1>
      <span className="text-xl">Register a new account.</span>
      <span className="pb-5">
        Already have an account?{" "}
        <Link href="/sign-in" className="hover:underline font-semibold">
          Sign in
        </Link>
      </span>
      <SignUpForm />
    </div>
  );
}
