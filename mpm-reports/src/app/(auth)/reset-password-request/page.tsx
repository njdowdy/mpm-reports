import RequestPasswordResetForm from "@/components/auth/RequestPasswordResetForm";

export default function RequestPasswordReset() {
  return (
    <div className="mx-auto w-[700px] flex flex-col space-y-8 mt-10">
      <h1 className="text-2xl">Reset Your Password</h1>
      <RequestPasswordResetForm />
    </div>
  );
}
