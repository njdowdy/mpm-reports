import { getUserByIdWithActivityRecordsWithActivities } from "@/lib/api/users/queries";

import { columns } from "./columns";
import { DataTable } from "./data-table";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";
import { redirect } from "next/navigation";

export default async function ActivityRecords() {
  const user = await getCurrentUser();
  if (!user) redirect("/sign-in");

  const { rows: activityRecords } =
    await getUserByIdWithActivityRecordsWithActivities(user.id);

  return (
    <div className="container mx-auto py-10">
      {activityRecords ? (
        <div className="overflow-x-auto pl-1">
          <DataTable columns={columns} data={activityRecords} />
        </div>
      ) : (
        <p>No activity records found</p>
      )}
    </div>
  );
}
