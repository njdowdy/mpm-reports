"use client";

import { ColumnDef } from "@tanstack/react-table";
import { Dialog } from "@/components/ui/dialog";
import { DropdownDialog } from "@/components/shared/DialogItem";
import SuperColumn from "./super-column";
import UserAvatar from "@/components/auth/UserAvatar";
import { UserWithActivityRecordsWithActivities } from "@/lib/api/users/queries";
import { LocationEnum } from "@/lib/db/schema/activityRecords";

export interface ActivityWithRecord {
  activityRecord: UserWithActivityRecordsWithActivities;
}

// TODO: Stop table columns from shifting left when filter returns no records
// TODO: Make default sorting to be descending by logged date
export const columns: ColumnDef<UserWithActivityRecordsWithActivities>[] = [
  //?: For nested objects, the accessor needs to be "tableName.field" instead of just "field"
  {
    accessorKey: "quarter",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Quarter" sortable={true} />
    ),
  },
  {
    accessorKey: "activity.name",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Activity" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.activity.name;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "activity.codes",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="CODES" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.activity.codes;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "activity.activityGroup",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Group" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.activity.activityGroup;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "description",
    header: () => <SuperColumn columnLabel="Description" sortable={false} />,
  },
  {
    accessorKey: "otherPartiesToActivityRecords",
    header: () => <SuperColumn columnLabel="Other Parties" sortable={false} />,
    //? Note: the return is more complicated because the types have been nested from the Drizzle Query
    cell: ({ row }) => {
      const val = row.original.otherPartiesToActivityRecords.map(
        ({ otherParty }) => otherParty,
      );
      return (
        <div className="flex">
          {val.map((otherParty, index: number) => (
            <div
              key={`${row.original.activityId}-${otherParty.id}`}
              className={`-ml-2 first:ml-0 z-${index * 5}`}
            >
              <UserAvatar
                user={otherParty}
                renderTooltip={true}
                variant={"bordered"}
              />
            </div>
          ))}
        </div>
      );
    },
  },
  {
    accessorKey: "location",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Location" sortable={true} />
    ),
    //?: For nested objects, the cell data can be accessed as row.original.tableName.field rather than as row.getValues("field")
    cell: ({ row }) => {
      const val: LocationEnum = row.original.location;
      const formatted = val
        .toString()
        .split("_")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "datePerformedStart",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Performed" sortable={true} />
    ),
    cell: ({ row }) => {
      const val1 = row.original.datePerformedStart
        .toLocaleDateString("en-US")
        .split("/")
        .splice(0, 2)
        .join("/");
      const val2 = row.original.datePerformedEnd
        .toLocaleDateString("en-US")
        .split("/")
        .splice(0, 2)
        .join("/");
      return <div className="font-medium">{`${val1} - ${val2}`}</div>;
    },
  },
  {
    accessorKey: "createdAt",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Logged" sortable={true} />
    ),
    cell: ({ row }) => {
      const val: Date = row.original.createdAt;
      const formatted = new Date(val).toLocaleDateString("en-US");
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    id: "actions",
    cell: ({ row }) => {
      const activityRecord = row.original;

      return (
        <Dialog>
          <DropdownDialog {...activityRecord} />
        </Dialog>
      );
    },
  },
];
