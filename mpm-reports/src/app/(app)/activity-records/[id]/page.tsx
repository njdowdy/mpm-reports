import { getActivityRecordsByIdWithRelations } from "@/lib/api/activityRecords/queries";
import { notFound } from "next/navigation";
import "server-only";

export interface ActivityRecordDetailsProps {
  id: string;
}

export default async function ActivityRecordDetails({
  params,
}: {
  params: ActivityRecordDetailsProps;
}) {
  const { rows: activityRecord } = await getActivityRecordsByIdWithRelations(
    params.id,
  );

  if (activityRecord === undefined) {
    return notFound();
  }

  return (
    <div className="container flex flex-col space-y-2 md:py-6 text-2xl">
      {/* //TODO: Improve this view */}
      <h1 className="pt-6 pb-3 text-5xl font-bold">
        Activity Record Details (Work in Progress)
      </h1>
      <span>ID: {activityRecord.id}</span>
      <span>Activity: {activityRecord.activity.name}</span>
      <span>Activity Group: {activityRecord.activity.activityGroup}</span>
      <span>CODES: {activityRecord.activity.codes}</span>
      <span>
        Quantity: {activityRecord.quantity} {activityRecord.unit.unit}
      </span>
      <span>Description: {activityRecord.description}</span>
      <span>Reported On: {activityRecord.createdAt.toLocaleDateString()}</span>
      <span>
        Reported By: {activityRecord.reportingUser.firstName}{" "}
        {activityRecord.reportingUser.lastName} (
        {activityRecord.reportingUser.username})
      </span>
      <span>
        Activity Start Date:{" "}
        {activityRecord.datePerformedStart.toLocaleDateString()}
      </span>
      <span>
        Activity End Date:{" "}
        {activityRecord.datePerformedEnd.toLocaleDateString()}
      </span>
      <span>Quarter: {activityRecord.quarter}</span>
      <span>
        Department:{" "}
        {activityRecord.department.name.charAt(0).toLocaleUpperCase() +
          activityRecord.department.name.slice(1)}
      </span>
      <span>Location: {activityRecord.location}</span>
      <span>Delivery Method: {activityRecord.deliveryMethod}</span>
      <span>
        Tags:{" "}
        {activityRecord.tagsToActivityRecords
          .map((tag) => tag.tag.tag)
          .join(", ")}
      </span>
    </div>
  );
}
