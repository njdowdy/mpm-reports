import { Button } from "@/components/ui/button";
import { ArrowUpDown } from "lucide-react";
import { Column } from "@tanstack/react-table";
import { cva, type VariantProps } from "class-variance-authority";

import { cn } from "@/lib/utils";

//? this is a custom component to customize the columns array of the data-table

const superColumnVariants = cva("border-none", {
  variants: {
    icon: {
      default: "bg-transparent text-foreground-muted",
    },
    iconSize: {
      default: "h-4 w-4",
    },
    header: {
      default: "ml-2 flex items-center tracking-tight text-nowrap",
    },
  },
  defaultVariants: {
    icon: "default",
    iconSize: "default",
    header: "default",
  },
});

export interface SuperColumnProps<T>
  extends VariantProps<typeof superColumnVariants> {
  column?: Column<T, unknown>;
  columnLabel: string;
  sortable?: boolean;
  className?: string;
}

export default function SuperColumn<T>({
  column,
  columnLabel,
  sortable = true,
  icon,
  iconSize,
  header,
  className,
}: SuperColumnProps<T>) {
  if (sortable === true) {
    return (
      <div className={cn(superColumnVariants({ header }))}>
        <p>{columnLabel}</p>
        <Button
          size={"icon"}
          variant={"outline"}
          className={cn(superColumnVariants({ icon, iconSize, className }))}
          onClick={() =>
            column?.toggleSorting(column?.getIsSorted() === "asc", true)
          }
        >
          <ArrowUpDown />
        </Button>
      </div>
    );
  }
  return (
    <div className={cn(superColumnVariants({ header }))}>{columnLabel}</div>
  );
}
