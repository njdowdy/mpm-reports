import "server-only";

export default function TermsOfService() {
  return (
    <>
      <div className="p-6 max-w-4xl mx-auto">
        <h1 className="text-2xl font-semibold mb-4">Terms of Service</h1>
        <p>Last updated: April 16, 2024</p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          1. Acceptance of Terms
        </h2>
        <p>
          By accessing and using our application, you agree to be bound by these
          Terms of Service (&quot;Terms&quot;). If you do not agree to these
          Terms, you must not use our application.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          2. Description of Service
        </h2>
        <p>
          Our application provides tools for tracking and reporting quarterly
          activities that contribute to your department&apos;s annual report.
          This service is intended to assist in the aggregation and analysis of
          departmental activities.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">3. Your Account</h2>
        <p>
          You are responsible for maintaining the confidentiality of your
          account and password and for restricting access to your computer. You
          agree to accept responsibility for all activities that occur under
          your account or password.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">4. Privacy Policy</h2>
        <p>
          Please review our Privacy Policy, which also governs your use of our
          application, to understand our practices.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          5. Modifications to Service
        </h2>
        <p>
          We reserve the right at any time to modify or discontinue, temporarily
          or permanently, the service (or any part of it) with or without
          notice.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">6. Termination</h2>
        <p>
          We may terminate your access to our application without cause or
          notice, which may result in the forfeiture and destruction of all
          information associated with your account.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">7. Governing Law</h2>
        <p>
          These Terms shall be governed by and construed in accordance with the
          laws of your jurisdiction, without regard to its conflict of law
          provisions.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">8. Changes to Terms</h2>
        <p>
          We reserve the right, at our sole discretion, to modify or replace
          these Terms at any time. If a revision is material, we will try to
          provide at least 30 days&apos; notice prior to any new terms taking
          effect. What constitutes a material change will be determined at our
          sole discretion.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">9. Contact Us</h2>
        <p>If you have any questions about these Terms, please contact us.</p>
      </div>
    </>
  );
}
