"use client";

import { ColumnDef } from "@tanstack/react-table";
import SuperColumn from "../activity-records/super-column";
import { ActivitiesWithRelations } from "@/lib/api/activities/queries";

// TODO: Stop table columns from shifting left when filter returns no records
export const columns: ColumnDef<ActivitiesWithRelations>[] = [
  //?: For nested objects, the accessor needs to be "tableName.field" instead of just "field"
  {
    accessorKey: "codes",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="CODES" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.codes;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
    filterFn: (row, columnId, filterValue) => {
      if (
        row.original.codes.toLowerCase().startsWith(filterValue.toLowerCase())
      ) {
        return true;
      }
      return false;
    },
  },
  {
    accessorKey: "activityGroup",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Group" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.activityGroup;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "name",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Activity" sortable={true} />
    ),
  },
  {
    accessorKey: "unit",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Unit(s)" sortable={false} />
    ),
    cell: ({ row }) => {
      const val = row.original.activitiesToQuantities
        ?.map((a) => a.quantity.unit)
        .join(", ");
      return <div className="font-medium">{val}</div>;
    },
  },
  {
    accessorKey: "locationImportant",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Location" sortable={false} />
    ),
    cell: ({ row }) => {
      const val = row.original.locationImportant.toString();
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "deliveryMethodImportant",
    header: ({ column }) => (
      <SuperColumn
        column={column}
        columnLabel="Delivery Method"
        sortable={false}
      />
    ),
    cell: ({ row }) => {
      const val = row.original.deliveryMethodImportant.toString();
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
];
