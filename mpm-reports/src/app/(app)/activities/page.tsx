import NewActivityForm from "@/components/activities/NewActivityForm";
import { getActivitiesByDepartmentWithRelations } from "@/lib/api/activities/queries";
import { getDepartmentsByUserId } from "@/lib/api/departments/queries";
import { getQuantities } from "@/lib/api/quantities/queries";
import { DataTable } from "./data-table";
import { columns } from "./columns";
import "server-only";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";
import { redirect } from "next/navigation";

export default async function ActivitiesPage() {
  const user = await getCurrentUser();
  if (!user) redirect("/sign-in");

  const { departments } = await getDepartmentsByUserId(user.id);
  // TODO: this needs to be filtered like the ReportNewActivity form dropdown data
  const { rows: activities } = await getActivitiesByDepartmentWithRelations(
    departments || [],
  );
  const { quantities } = await getQuantities();

  const departmentActivities = activities?.map((activity) => activity) || [];

  return (
    <div className="flex flex-col space-y-6 max-w-[900px] mx-auto mt-10">
      <div className="flex-col flex space-y-2">
        <h1 className="text-4xl font-bold">
          Current Activities in Your Department
        </h1>
        {activities ? (
          <div className="pl-1">
            <DataTable columns={columns} data={activities} />
          </div>
        ) : (
          <p>No reports data found</p>
        )}
      </div>
      <h1 className="text-4xl font-bold mb-6">Add A New Activity</h1>
      <NewActivityForm
        departmentOptions={departments || []}
        quantityOptions={quantities}
      />
    </div>
  );
}
