import "server-only";
import ThemeSwitcher from "@/components/settings/ThemeSwitcher";

export default async function SettingsPage() {
  return (
    <div className="flex flex-col container md:py-6">
      <h1 className="text-3xl font-bold py-6">Settings</h1>
      <div className="flex flex-col space-y-6">
        <ThemeSwitcher />
      </div>
    </div>
  );
}
