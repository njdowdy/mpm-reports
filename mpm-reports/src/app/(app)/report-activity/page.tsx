import Loading from "@/app/loading";
import ReportNewActivityForm from "@/components/activityRecords/ReportNewActivityForm";
import {
  ActivitiesWithRelations,
  getActivitiesByDepartmentWithRelations,
} from "@/lib/api/activities/queries";
import { getDepartmentsByUserId } from "@/lib/api/departments/queries";
import { getTags } from "@/lib/api/tags/queries";
import { getUsersByDepartments } from "@/lib/api/users/queries";
import { Department } from "@/lib/db/schema/departments";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";
import { redirect } from "next/navigation";
import { Suspense } from "react";

function filterActivities(
  activities: ActivitiesWithRelations[],
  departmentList: Department[],
): ActivitiesWithRelations[] {
  return activities.filter((activity) =>
    activity.activitiesToDepartments.some((atd) =>
      departmentList.some((dept) => dept.name === atd.department.name),
    ),
  );
}

export default async function ReportActivityPage() {
  const user = await getCurrentUser();
  if (!user) redirect("/sign-in");
  const { departments } = await getDepartmentsByUserId(user.id);
  //? Get all activities from current user's departments
  //Function is NOT filtering results by department; temporary solution: get all activities and filter them in the frontend
  const { rows: activities } = await getActivitiesByDepartmentWithRelations(
    departments || [],
  );

  const filteredActivities = filterActivities(
    activities || [],
    departments || [],
  );

  //? Get all users from current user's departments
  const { users: otherDepartmentUsers } = await getUsersByDepartments(
    departments || [],
  );
  //? Get all users from current user's departments
  const { tags } = await getTags();
  const simplifiedTags = tags.map(({ createdAt, updatedAt, ...rest }) => rest);

  return (
    <div className="container my-10">
      <h1 className="text-4xl font-bold mb-10 text-center">
        Report New Activity
      </h1>
      <Suspense fallback={<Loading />}>
        <ReportNewActivityForm
          reportingUser={user}
          activitiesOptions={
            filteredActivities?.sort((a, b) => a.name.localeCompare(b.name)) ||
            []
          }
          otherPartiesOptions={
            otherDepartmentUsers?.map((otherUser) => otherUser.user) || []
          }
          tagsOptions={simplifiedTags}
        />
      </Suspense>
    </div>
  );
}
