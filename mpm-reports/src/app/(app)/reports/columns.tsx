"use client";

import { ColumnDef } from "@tanstack/react-table";
import SuperColumn from "../activity-records/super-column";
import { QuarterlyActivitySummary } from "@/lib/api/activityRecords/queries";
import Link from "next/link";
import { Button } from "@/components/ui/button";
import { LinkIcon } from "lucide-react";

// TODO: Stop table columns from shifting left when filter returns no records
// TODO: Make default sorting to be descending by fiscal year / quarter
export const columns: ColumnDef<QuarterlyActivitySummary>[] = [
  //?: For nested objects, the accessor needs to be "tableName.field" instead of just "field"
  {
    accessorKey: "quarter",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Quarter" sortable={true} />
    ),
  },
  {
    accessorKey: "monthRange",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Month Range" sortable={false} />
    ),
    cell: ({ row }) => {
      const val = row.original.quarter;
      const quarter = val.split("Q")[1];
      if (quarter === "1") {
        return <div className="font-medium">September - November</div>;
      } else if (quarter === "2") {
        return <div className="font-medium">December - February</div>;
      } else if (quarter === "3") {
        return <div className="font-medium">March - May</div>;
      } else if (quarter === "4") {
        return <div className="font-medium">June - August</div>;
      }
    },
  },
  {
    accessorKey: "activityRecordsCount",
    header: ({ column }) => (
      <SuperColumn
        column={column}
        columnLabel="Activity Count"
        sortable={true}
      />
    ),
  },
  {
    accessorKey: "details",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Details" sortable={false} />
    ),
    cell: ({ row }) => {
      const val = row.original.quarter;
      const details = {
        year: val.split("Q")[0].replace("FY", ""),
        quarter: val.split("Q")[1],
      };
      return (
        <Button variant={"link"} asChild>
          <Link href={`/reports/${details.year}/${details.quarter}`}>
            <LinkIcon className="w-4 h-4" />
          </Link>
        </Button>
      );
    },
  },
];
