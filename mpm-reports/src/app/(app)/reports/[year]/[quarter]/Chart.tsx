"use client";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip);

interface DoughnutProps {
  data: {
    labels: string[];
    datasets: {
      label: string;
      data: number[];
      backgroundColor: string[];
      hoverOffset: number;
    }[];
  };
}

// TODO: on hover, the chart grows slightly and expands past its container, creating a black border around the chart
export default function DoughnutComponent({ data }: DoughnutProps) {
  return <Doughnut className="h-[250px] w-[250px]" data={data} />;
}
