"use client";

import { ColumnDef } from "@tanstack/react-table";
import SuperColumn from "../../../activity-records/super-column";
import { ActivityRecordsWithRelations } from "@/lib/api/activityRecords/queries";
import { LocationEnum } from "@/lib/db/schema/activityRecords";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { LinkIcon } from "lucide-react";

// TODO: Stop table columns from shifting left when filter returns no records
export const columns: ColumnDef<ActivityRecordsWithRelations>[] = [
  //?: For nested objects, the accessor needs to be "tableName.field" instead of just "field"
  {
    accessorKey: "department.name",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Department" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.department.name.replace("_", " ");
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "activity.codes",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="CODES" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.activity.codes;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "activity.activityGroup",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Group" sortable={true} />
    ),
    cell: ({ row }) => {
      const val = row.original.activity.activityGroup;
      const formatted = val.charAt(0).toUpperCase() + val.slice(1);
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "activity.name",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Activity" sortable={true} />
    ),
  },
  {
    accessorKey: "quantity",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Quantity" sortable={true} />
    ),
  },
  {
    accessorKey: "unit.unit",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Unit" sortable={true} />
    ),
  },
  {
    accessorKey: "location",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Location" sortable={true} />
    ),
    cell: ({ row }) => {
      const val: LocationEnum = row.original.location;
      const formatted = val
        .toString()
        .split("_")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
      return <div className="font-medium">{formatted}</div>;
    },
  },
  {
    accessorKey: "datePerformedStart",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Performed" sortable={true} />
    ),
    cell: ({ row }) => {
      const val1 = row.original.datePerformedStart
        .toLocaleDateString("en-US")
        .split("/")
        .splice(0, 2)
        .join("/");
      const val2 = row.original.datePerformedEnd
        .toLocaleDateString("en-US")
        .split("/")
        .splice(0, 2)
        .join("/");
      return <div className="font-medium">{`${val1} - ${val2}`}</div>;
    },
  },
  {
    accessorKey: "details",
    header: ({ column }) => (
      <SuperColumn column={column} columnLabel="Details" sortable={false} />
    ),
    cell: ({ row }) => {
      const val = row.original.id;
      return (
        <Button variant={"link"} asChild>
          <Link href={`/activity-records/${val}`}>
            <LinkIcon className="w-4 h-4" />
          </Link>
        </Button>
      );
    },
  },
];
