import "server-only";
import { ActivityRecordsWithRelations } from "@/lib/api/activityRecords/queries";
import DoughnutComponent from "./Chart";
import { cn } from "@/lib/utils";

export interface DepartmentActivityCardProps {
  activityRecords: ActivityRecordsWithRelations[];
  department: string;
  className?: string;
}

export default async function DepartmentActivityCard({
  activityRecords,
  department,
  className,
}: DepartmentActivityCardProps) {
  // console.log(activityRecords);
  const countTours = activityRecords.filter((record) =>
    record.activity.name.toLocaleLowerCase().includes("tour"),
  ).length;

  const countPubs = activityRecords.filter((record) =>
    record.activity.activityGroup.toLocaleLowerCase().includes("pub"),
  ).length;

  const collectionsCount = activityRecords.filter((record) =>
    record.activity.codes.toLocaleLowerCase().includes("collections"),
  ).length;

  const organizationalCultureCount = activityRecords.filter((record) =>
    record.activity.codes.toLocaleLowerCase().includes("organization"),
  ).length;

  const discoveryCount = activityRecords.filter((record) =>
    record.activity.codes.toLocaleLowerCase().includes("discovery"),
  ).length;

  const engagementCount = activityRecords.filter((record) =>
    record.activity.codes.toLocaleLowerCase().includes("engagement"),
  ).length;

  const serviceCount = activityRecords.filter((record) =>
    record.activity.codes.toLocaleLowerCase().includes("service"),
  ).length;

  const data = {
    labels: [
      "COLLECTIONS",
      "ORGANIZATIONAL CULTURE",
      "DISCOVERY",
      "ENGAGEMENT",
      "SERVICE",
    ],
    datasets: [
      {
        label: "Activity Count",
        data: [
          collectionsCount,
          organizationalCultureCount,
          discoveryCount,
          engagementCount,
          serviceCount,
        ],
        backgroundColor: [
          "#648FFF",
          "#785EF0",
          "#DC267F",
          "#FE6100",
          "#FFB000",
        ],
        hoverOffset: 4,
      },
    ],
  };

  return (
    <div className={cn("border-[1px] rounded-lg p-4 max-w-[500px]", className)}>
      <h4 className="text-2xl font-bold pb-4">{`${department.toLocaleUpperCase().replace("_", " ")}`}</h4>
      <div className="flex space-x-16">
        <div className="text-lg">
          <p>Publications: {countPubs}</p>
          <p>Highlight #2: TBD</p>
          <p>Highlight #3: TBD</p>
          <p>Highlight #4: TBD</p>
        </div>
        <div>
          <DoughnutComponent data={data} />
        </div>
      </div>
    </div>
  );
}
