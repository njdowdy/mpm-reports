import "server-only";
import { notFound, redirect } from "next/navigation";
import { getActivityRecordsByQuarterWithRelations } from "@/lib/api/activityRecords/queries";
import { DataTable } from "./data-table";
import { columns } from "./columns";
import DepartmentActivityCard from "./DepartmentActivityCard";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { ArrowLeftIcon, ArrowRightIcon } from "lucide-react";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";

export interface QuarterDetailsProps {
  year: string;
  quarter: string;
}

//! TODO: There is a bug using "Next Quarter" button from FY2024Q2 to FY2024Q3 in production
export default async function QuarterDetails({
  params,
}: {
  params: { year: string; quarter: string };
}) {
  const quarter = `FY${params.year}Q${params.quarter}`;

  const sessionUser = await getCurrentUser();
  if (!sessionUser) redirect("/sign-in");
  const { rows: quarterData } =
    await getActivityRecordsByQuarterWithRelations(quarter);

  const departmentList = Array.from(
    new Set(
      quarterData?.flatMap((record) =>
        record.department.name.toLocaleLowerCase(),
      ),
    ),
  ).sort();

  if (params.year.startsWith("20") === false || params.year.length !== 4) {
    return notFound();
  }

  let startMonth = undefined;
  let endMonth = undefined;
  let nextQuarter = undefined;
  let previousQuarter = undefined;
  let nextYear = undefined;
  let previousYear = undefined;
  if (params.quarter === "1") {
    startMonth = "September";
    endMonth = "November";
    nextQuarter = "2";
    nextYear = params.year;
    previousQuarter = "4";
    previousYear = `${Number(params.year) - 1}`;
  } else if (params.quarter === "2") {
    startMonth = "December";
    endMonth = "February";
    nextQuarter = "3";
    nextYear = params.year;
    previousQuarter = "1";
    previousYear = params.year;
  } else if (params.quarter === "3") {
    startMonth = "March";
    endMonth = "May";
    nextQuarter = "4";
    nextYear = params.year;
    previousQuarter = "2";
    previousYear = params.year;
  } else if (params.quarter === "4") {
    startMonth = "June";
    endMonth = "August";
    nextQuarter = "1";
    nextYear = `${Number(params.year) + 1}`;
    previousQuarter = "3";
    previousYear = params.year;
  } else {
    return notFound();
  }

  return (
    <div className="container flex flex-col md:py-6">
      <h1 className="pt-6 pb-3 text-4xl font-bold">
        Academic Programs Quarterly Report
      </h1>
      <h2 className="text-xl font-bold">
        {quarter} - {startMonth} {params.year} through {endMonth} {params.year}
      </h2>
      <div className="flex pb-8 space-x-4">
        <Button variant={"link"} className="px-0">
          <Link href={`/reports/${previousYear}/${previousQuarter}`}>
            <div className="flex space-x-3 items-center">
              <ArrowLeftIcon />
              <span>Previous Quarter</span>
            </div>
          </Link>
        </Button>
        <Button variant={"link"}>
          <Link href={`/reports/${nextYear}/${nextQuarter}`}>
            <div className="flex space-x-3 items-center">
              <span>Next Quarter</span>
              <ArrowRightIcon />
            </div>
          </Link>
        </Button>
      </div>
      <div className="flex flex-col space-y-6">
        <h3 className="text-3xl font-bold">Activity Highlights</h3>
        {quarterData && (
          <DepartmentActivityCard
            key={"all_departments"}
            department={"MPM"}
            activityRecords={quarterData}
          />
        )}
        <h3 className="text-3xl font-bold">
          Activity Highlights by Department
        </h3>
        <div className="flex flex-wrap gap-3">
          {quarterData ? (
            departmentList.map((department) => {
              const filteredQuarterData = quarterData.filter(
                (record) =>
                  record.department.name.toLocaleLowerCase() === department,
              );
              return (
                <DepartmentActivityCard
                  key={department}
                  department={department}
                  activityRecords={filteredQuarterData}
                />
              );
            })
          ) : (
            <p>No reports data found</p>
          )}
        </div>
        {quarterData && (
          <div className="pl-1 overflow-x-auto">
            <DataTable columns={columns} data={quarterData} />
          </div>
        )}
      </div>
    </div>
  );
}
