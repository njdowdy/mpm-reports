import { getQuarterlyActivitySummary } from "@/lib/api/activityRecords/queries";

import { columns } from "./columns";
import { DataTable } from "./../activity-records/data-table";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";
import { redirect } from "next/navigation";

export default async function QuarterReports() {
  const user = await getCurrentUser();
  if (!user) redirect("/sign-in");

  const { rows: activityRecords } = await getQuarterlyActivitySummary();

  return (
    <div className="container mx-auto py-10">
      <h1 className="text-5xl font-bold py-6">All Reports</h1>
      {activityRecords ? (
        <div className="overflow-x-auto pl-1">
          <DataTable columns={columns} data={activityRecords} />
        </div>
      ) : (
        <p>No reports data found</p>
      )}
    </div>
  );
}
