import ProfileCard from "@/components/profile/ProfileCard";
import { getUserByUsername } from "@/lib/api/users/queries";
import { Suspense } from "react";
import CurrentDepartmentList from "@/components/departments/CurrentDepartmentList";
import DepartmentRequest from "@/components/departments/DepartmentRequest";
import AvatarImageUpload from "@/components/users/AvatarImageUpload";
import {
  getDepartments,
  getDepartmentsByUserId,
} from "@/lib/api/departments/queries";
import "server-only";
import { User } from "@/lib/db/schema/users";
import { Department } from "@/lib/db/schema/departments";
import { redirect } from "next/navigation";
import { getCurrentUser } from "@/lib/supabase/auth/server/actions";

export interface ProfileProps {
  username: string;
}

export default async function Profile({
  params,
}: {
  params: { username: string };
}) {
  const sessionUser = await getCurrentUser();
  if (!sessionUser) redirect("/sign-in");
  const { user: profileUser } = await getUserByUsername(params.username);

  if (!profileUser) {
    return <div>User not found</div>;
  }
  const isOwner = sessionUser?.id === profileUser?.id;
  const { departments: userDepts } = await getDepartmentsByUserId(
    profileUser.id,
  );

  return (
    <div className="flex flex-col container md:py-6">
      <h1 className="text-3xl font-bold py-6">Profile Details</h1>
      <div className="flex flex-col space-y-6">
        <BaseProfile profileUser={profileUser} userDepts={userDepts ?? []} />
        {isOwner && <OwnerProfile userDepts={userDepts ?? []} />}
      </div>
    </div>
  );
}

async function OwnerProfile({ userDepts }: { userDepts: Department[] }) {
  const { departments: allDepts } = await getDepartments();

  const nonOverlappingDepts = allDepts.filter(
    (dept) => !userDepts?.some((userDept) => userDept.id === dept.id),
  );
  return (
    <>
      <Suspense fallback={<div>Loading...</div>}>
        <DepartmentRequest departmentList={nonOverlappingDepts || []} />
      </Suspense>
      <Suspense fallback={<div>Loading...</div>}>
        <AvatarImageUpload />
      </Suspense>
    </>
  );
}

async function BaseProfile({
  profileUser,
  userDepts,
}: {
  profileUser: User;
  userDepts: Department[];
}) {
  return (
    <>
      <Suspense fallback={<div className="min-h-[125px]">Loading...</div>}>
        <ProfileCard user={profileUser} />
      </Suspense>
      <Suspense fallback={<div>Loading...</div>}>
        <CurrentDepartmentList
          isOwner={true}
          departmentList={userDepts || []}
        />
      </Suspense>
    </>
  );
}
