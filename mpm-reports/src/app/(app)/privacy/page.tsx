import "server-only";

export default function PrivacyPolicy() {
  return (
    <>
      <div className="p-6 max-w-4xl mx-auto">
        <h1 className="text-2xl font-semibold mb-4">Privacy Policy</h1>
        <p>Last updated: April 16, 2024</p>
        <h2 className="text-xl font-semibold mt-6 mb-2">1. Introduction</h2>
        <p>
          We are committed to protecting your personal information and your
          right to privacy. If you have any questions or concerns about our
          policy, or our practices with regards to your personal information,
          please contact us.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          2. Information We Collect
        </h2>
        <p>
          As you use our application, we collect information about you and your
          use of our service. We collect information that you provide directly
          to us when you register, update your profile, perform account
          activities, and more.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          3. How We Use Your Information
        </h2>
        <p>We use the information we collect for various purposes:</p>
        <ul className="list-disc pl-6">
          <li>To provide and maintain our service</li>
          <li>To improve our service</li>
          <li>To monitor the usage of our service</li>
          <li>To detect, prevent, and address technical issues</li>
        </ul>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          4. Sharing Your Personal Information
        </h2>
        <p>
          We do not share or sell your personal information with third parties
          except to comply with legal obligations, to protect and defend our
          rights or property, or to prevent or investigate possible wrongdoing
          in connection with the service.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          5. Security of Your Information
        </h2>
        <p>
          We take your security seriously and implement appropriate security
          measures to protect your personal information. However, no
          internet-based service is 100% secure, so we cannot guarantee the
          absolute security of your information.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          6. Children&apos;s Privacy
        </h2>
        <p>
          Our service does not address anyone under the age of 13. We do not
          knowingly collect personally identifiable information from anyone
          under the age of 13. If we become aware that we have collected
          personal information from children without verification of parental
          consent, we take steps to remove that information from our servers.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">
          7. Changes to This Privacy Policy
        </h2>
        <p>
          We may update our Privacy Policy from time to time. We will notify you
          of any changes by posting the new Privacy Policy on this page.
        </p>
        <h2 className="text-xl font-semibold mt-6 mb-2">8. Contact Us</h2>
        <p>
          If you have any questions about this Privacy Policy, you can contact
          us.
        </p>
      </div>
    </>
  );
}
