import AuthModal from "@/components/AuthModal";
import Modal from "@/components/Modal";

export default function SignIn() {
  return (
    <Modal>
      <AuthModal />
    </Modal>
  );
}
