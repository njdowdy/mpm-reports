// Importing env files here to validate on build
import "./src/lib/env.mjs";

import withBundleAnalyzer from "@next/bundle-analyzer";

/** @type {import('next').NextConfig} */
const bundleAnalyzer = withBundleAnalyzer({
  enabled: process.env.ANALYZE === "true",
});
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "localhost",
        port: "3000",
        pathname: "/**",
      },
      {
        protocol: "https",
        hostname: `${process.env.NEXT_PUBLIC_SUPABASE_URL}`,
        port: "",
        pathname: "/**",
      },
    ],
  },
  experimental: {
    webpackBuildWorker: true,
  },
};
export default bundleAnalyzer(nextConfig);
